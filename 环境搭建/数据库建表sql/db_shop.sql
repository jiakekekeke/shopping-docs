/*
 Navicat Premium Data Transfer

 Source Server         : 192.168.31.142
 Source Server Type    : MySQL
 Source Server Version : 50721
 Source Host           : 192.168.31.142:3306
 Source Schema         : db_shop

 Target Server Type    : MySQL
 Target Server Version : 50721
 File Encoding         : 65001

 Date: 29/03/2020 22:12:07
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for shop_address
-- ----------------------------
DROP TABLE IF EXISTS `shop_address`;
CREATE TABLE `shop_address`  (
  `C_ID` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `C_SJR_ID` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '收件人编号',
  `C_PROVINCE` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '收件地址_省',
  `C_CITY` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '收件地址_市',
  `C_AREA` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '收件地址_区',
  `C_XXDZ` varchar(600) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '详细地址',
  `C_PHONE` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号码',
  `C_ZIPMAP` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮政编码',
  `C_CJR` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `DT_CJSJ` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `C_GXR` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `DT_GXSJ` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`C_ID`) USING BTREE,
  INDEX `I_PK_SHOP_ADDRESS`(`C_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '收货地址表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for shop_cart
-- ----------------------------
DROP TABLE IF EXISTS `shop_cart`;
CREATE TABLE `shop_cart`  (
  `C_ID` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号',
  `C_GOOD_ID` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品编号',
  `C_ORDER_ID` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '订单编号',
  `C_CJR` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `DT_CJSJ` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `C_GXR` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `DT_GXSJ` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`C_ID`) USING BTREE,
  INDEX `IDX_CART_GOODID`(`C_GOOD_ID`) USING BTREE,
  INDEX `I_PK_SHOP_CART`(`C_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '购物信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for shop_comment
-- ----------------------------
DROP TABLE IF EXISTS `shop_comment`;
CREATE TABLE `shop_comment`  (
  `C_ID` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号',
  `C_ID_AJXX` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '评价人名称',
  `N_PJJB` int(11) NULL DEFAULT NULL COMMENT '评价级别',
  `C_IP` varchar(600) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '评价内容',
  `C_CJR` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `DT_CJSJ` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `C_GXR` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `DT_GXSJ` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `C_GOOD_ID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品id',
  PRIMARY KEY (`C_ID`) USING BTREE,
  INDEX `I_PK_SHOP_COMMENT`(`C_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商品评价表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for shop_feedback
-- ----------------------------
DROP TABLE IF EXISTS `shop_feedback`;
CREATE TABLE `shop_feedback`  (
  `C_ID` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `C_TITLE` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `C_SSDRMC` varchar(600) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '反馈信息',
  `C_CJR` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `DT_CJSJ` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `C_GXR` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `DT_GXSJ` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`C_ID`) USING BTREE,
  INDEX `I_PK_SHOP_FEEDBACK`(`C_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '意见反馈表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for shop_good
-- ----------------------------
DROP TABLE IF EXISTS `shop_good`;
CREATE TABLE `shop_good`  (
  `C_ID` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号',
  `N_GOOD_LX` int(11) NULL DEFAULT NULL COMMENT '商品类型',
  `C_GOOD_YTLX` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品用途类型',
  `C_GOOD_MC` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `C_DPMC` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '店铺名称',
  `C_WEIGHT` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '毛重',
  `C_ORIGIN` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '产地',
  `N_MATERIAL` int(11) NULL DEFAULT NULL COMMENT '材质',
  `N_STYLE` int(11) NULL DEFAULT NULL COMMENT '款式',
  `N_PEOPLE` int(11) NULL DEFAULT NULL COMMENT '适用人群',
  `N_PRICE` decimal(17, 2) NULL DEFAULT NULL COMMENT '价格',
  `N_PRICE_YH` decimal(17, 2) NULL DEFAULT NULL COMMENT '优惠价',
  `N_SFCX` int(11) NULL DEFAULT NULL COMMENT '是否促销',
  `N_SFMS` int(11) NULL DEFAULT NULL COMMENT '是否秒杀',
  `C_CJR` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `DT_CJSJ` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `C_IMG` varchar(600) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片地址',
  `C_PJS` int(11) NULL DEFAULT NULL COMMENT '评价数',
  PRIMARY KEY (`C_ID`) USING BTREE,
  INDEX `I_PK_SHOP_GOOD`(`C_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商品信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for shop_good_used
-- ----------------------------
DROP TABLE IF EXISTS `shop_good_used`;
CREATE TABLE `shop_good_used`  (
  `C_ID` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号',
  `N_GOOD_LX` int(11) NULL DEFAULT NULL COMMENT '商品类型',
  `C_GOOD_YT` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品用途',
  `N_LEVEL` int(11) NULL DEFAULT NULL COMMENT '等级',
  `N_ORDER` int(11) NULL DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`C_ID`) USING BTREE,
  INDEX `I_PK_SHOP_GOOD_USED`(`C_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商品用途分类表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for shop_order
-- ----------------------------
DROP TABLE IF EXISTS `shop_order`;
CREATE TABLE `shop_order`  (
  `C_ID` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号',
  `C_ORDER_BH` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '订单流水号',
  `C_ADDRESS` varchar(600) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '收货地址',
  `C_BZ` varchar(600) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `C_CJR` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `DT_CJSJ` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `C_GXR` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `DT_GXSJ` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`C_ID`) USING BTREE,
  INDEX `I_PK_SHOP_ORDER`(`C_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '订单信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for shop_search
-- ----------------------------
DROP TABLE IF EXISTS `shop_search`;
CREATE TABLE `shop_search`  (
  `C_ID` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号',
  `C_SSNR` varchar(600) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '搜索内容',
  `C_CJR` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `DT_CJSJ` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`C_ID`) USING BTREE,
  INDEX `I_PK_SHOP_SEARCH`(`C_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '搜索记录表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for shop_user
-- ----------------------------
DROP TABLE IF EXISTS `shop_user`;
CREATE TABLE `shop_user`  (
  `C_ID` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号',
  `C_LOGINID` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '登录名',
  `C_NAME` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `C_PASSWORD` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  `C_EMAIL` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `C_PHONE` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号码',
  `C_XMJP` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '姓名简拼',
  `N_VAILD` int(11) NULL DEFAULT NULL COMMENT '是否有效',
  `DT_CJSJ` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `DT_GXSJ` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `C_TXDZ` varchar(600) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像地址',
  PRIMARY KEY (`C_ID`) USING BTREE,
  INDEX `I_PK_SHOP_USER`(`C_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '人员表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_code
-- ----------------------------
DROP TABLE IF EXISTS `t_code`;
CREATE TABLE `t_code`  (
  `C_PID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '父级编号',
  `C_CODE` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '代码值',
  `C_NAME` varchar(600) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '代码名称',
  `C_LEVELINFO` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分级信息',
  `N_SFKWH` int(11) NULL DEFAULT NULL COMMENT '是否可维护',
  `N_VAILD` int(11) NULL DEFAULT NULL COMMENT '是否有效',
  `N_ORDER` int(11) NULL DEFAULT NULL COMMENT '序号'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '单值代码表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_codetype
-- ----------------------------
DROP TABLE IF EXISTS `t_codetype`;
CREATE TABLE `t_codetype`  (
  `C_ID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '编号',
  `C_NAME` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `N_SFKWH` int(11) NULL DEFAULT NULL COMMENT '是否可维护',
  `N_VAILD` int(11) NULL DEFAULT NULL COMMENT '是否有效'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '单值代码类型表' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
