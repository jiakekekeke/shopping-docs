/*
 Navicat Premium Data Transfer

 Source Server         : 192.168.31.142
 Source Server Type    : MySQL
 Source Server Version : 50721
 Source Host           : 192.168.31.142:3306
 Source Schema         : db_shop

 Target Server Type    : MySQL
 Target Server Version : 50721
 File Encoding         : 65001

 Date: 19/04/2020 12:36:33
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for shop_address
-- ----------------------------
DROP TABLE IF EXISTS `shop_address`;
CREATE TABLE `shop_address`  (
  `C_ID` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `C_SJR` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '收件人',
  `C_PROVINCE` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '收件地址_省',
  `C_CITY` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '收件地址_市',
  `C_AREA` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '收件地址_区',
  `C_XXDZ` varchar(600) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '详细地址',
  `C_PHONE` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号码',
  `C_ZIPMAP` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮政编码',
  `C_CJR` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `DT_CJSJ` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `C_GXR` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `DT_GXSJ` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`C_ID`) USING BTREE,
  INDEX `I_PK_SHOP_ADDRESS`(`C_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '收货地址表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of shop_address
-- ----------------------------
INSERT INTO `shop_address` VALUES ('1121321', '测试05', '广东省', '广东市', 'xxx区', ' 燕岭路633号', '13074139850', '713300', 'jk', NULL, NULL, NULL);
INSERT INTO `shop_address` VALUES ('11231', '测试03', '广东省', '广东市', 'xxx区', ' 燕岭路633号', '13074139850', '713300', 'jk', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for shop_cart
-- ----------------------------
DROP TABLE IF EXISTS `shop_cart`;
CREATE TABLE `shop_cart`  (
  `C_ID` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号',
  `C_GOOD_ID` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品编号',
  `C_ORDER_ID` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '订单编号',
  `C_CJR` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `DT_CJSJ` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `C_GXR` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `DT_GXSJ` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `N_GOODSNUM` int(11) NULL DEFAULT NULL COMMENT '商品数量',
  `N_PRICE` decimal(10, 2) NULL DEFAULT NULL COMMENT '价格',
  `N_SFFQ` int(11) NULL DEFAULT NULL COMMENT '是否分期',
  `N_FQQS` int(11) NULL DEFAULT NULL COMMENT '分期数',
  `N_CM` int(11) NULL DEFAULT NULL COMMENT '尺码',
  PRIMARY KEY (`C_ID`) USING BTREE,
  INDEX `IDX_CART_GOODID`(`C_GOOD_ID`) USING BTREE,
  INDEX `I_PK_SHOP_CART`(`C_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '购物信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of shop_cart
-- ----------------------------
INSERT INTO `shop_cart` VALUES ('085e7118b9594db9afef12ca8288a47c', '1', '20041210310001', 'jk', '2020-04-05 07:21:00', NULL, NULL, 2, 143.80, 2, 0, NULL);
INSERT INTO `shop_cart` VALUES ('15370de763a2410bb805710874d5ec62', '11', '20040419140001', 'jk', '2020-04-02 12:41:05', NULL, NULL, 1, 71.90, 1, -1, NULL);
INSERT INTO `shop_cart` VALUES ('3ff26710018f44f1aefc3d59eb1c1108', '1', '20040514060001', 'jk', '2020-04-04 15:31:32', NULL, NULL, NULL, 0.00, NULL, NULL, NULL);
INSERT INTO `shop_cart` VALUES ('4080bcd809e84e0982b2bfd350d1fdb7', '111', '20040419230001', 'jk', '2020-04-02 12:39:04', NULL, NULL, 1, 71.90, 2, 0, NULL);
INSERT INTO `shop_cart` VALUES ('569f0193080441d79916391d3a3bcc0a', '1', '20040423320001', 'jk', '2020-04-04 15:32:15', NULL, NULL, 1, 71.90, 2, 0, NULL);
INSERT INTO `shop_cart` VALUES ('5caf5135b3ab436ea3826db4a3280e99', '1', '20040423320001', 'jk', '2020-04-04 15:30:43', NULL, NULL, 1, 71.90, 2, 0, NULL);
INSERT INTO `shop_cart` VALUES ('735fdf3124d64b38a36a65e5e8f226e8', '1', '20041210310001', 'jk', '2020-04-05 06:23:44', NULL, NULL, 1, 71.90, 2, 0, NULL);
INSERT INTO `shop_cart` VALUES ('856aadfbe1fd43d8801b15633b17bd2e', '10', '20041210310001', 'jk', '2020-04-12 02:29:22', NULL, NULL, 1, 159.00, 2, 0, NULL);
INSERT INTO `shop_cart` VALUES ('9f5341d8a0174e8fa02df33953a2066e', '1', '20040423320001', 'jk', '2020-04-04 15:31:56', NULL, NULL, 1, 71.90, 2, 0, NULL);
INSERT INTO `shop_cart` VALUES ('d2309f53cb4f4d70853e1893e25d63a8', '1', '20040514060001', 'jk', '2020-04-04 15:30:43', NULL, NULL, 1, 71.90, 2, 0, NULL);
INSERT INTO `shop_cart` VALUES ('d793fcd76dc846bba399fdb4535c2b78', '1112', '20040514060001', 'jk', '2020-04-02 12:40:21', NULL, NULL, 1, 71.90, 2, 0, NULL);

-- ----------------------------
-- Table structure for shop_comment
-- ----------------------------
DROP TABLE IF EXISTS `shop_comment`;
CREATE TABLE `shop_comment`  (
  `C_ID` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号',
  `C_ID_USER` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '评价人名称',
  `N_PJJB` int(11) NULL DEFAULT NULL COMMENT '评价级别',
  `C_IP` varchar(600) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '评价内容',
  `C_CJR` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `DT_CJSJ` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `C_GXR` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `DT_GXSJ` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `C_GOOD_ID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品id',
  PRIMARY KEY (`C_ID`) USING BTREE,
  INDEX `I_PK_SHOP_COMMENT`(`C_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商品评价表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of shop_comment
-- ----------------------------
INSERT INTO `shop_comment` VALUES ('1', '啊发发发', 2, '是傻瓜式的恢复规划法规和法国恢复', '啊发发发', '2020-05-06 15:13:23', NULL, NULL, '1');

-- ----------------------------
-- Table structure for shop_feedback
-- ----------------------------
DROP TABLE IF EXISTS `shop_feedback`;
CREATE TABLE `shop_feedback`  (
  `C_ID` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `C_TITLE` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `C_SSDRMC` varchar(600) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '反馈信息',
  `C_CJR` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `DT_CJSJ` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `C_GXR` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `DT_GXSJ` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`C_ID`) USING BTREE,
  INDEX `I_PK_SHOP_FEEDBACK`(`C_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '意见反馈表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of shop_feedback
-- ----------------------------
INSERT INTO `shop_feedback` VALUES ('1195a3b7d24642a88e77746ff8dc31d7', '11', '22', 'jk', '2020-04-04 07:42:15', NULL, NULL);
INSERT INTO `shop_feedback` VALUES ('5e2845c08db74fb2bac788e1ff97e032', '1', '1', 'jk', '2020-04-05 07:37:30', NULL, NULL);
INSERT INTO `shop_feedback` VALUES ('76a3e9f9a5994594b43c350319a723ba', '1', '1', 'jk', '2020-04-05 07:37:35', NULL, NULL);
INSERT INTO `shop_feedback` VALUES ('78dc96d08c2d43b88d77d30cc9cf33a3', '1', '1', 'jk', '2020-04-05 07:37:35', NULL, NULL);
INSERT INTO `shop_feedback` VALUES ('84a2d80757ac4ed2941a8be999b91799', '1', '1', 'jk', '2020-04-05 07:37:35', NULL, NULL);

-- ----------------------------
-- Table structure for shop_good
-- ----------------------------
DROP TABLE IF EXISTS `shop_good`;
CREATE TABLE `shop_good`  (
  `C_ID` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号',
  `N_GOOD_LX` int(11) NULL DEFAULT NULL COMMENT '一级类型',
  `C_GOOD_YTLX` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '二级类型',
  `C_GOOD_MC` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `C_DPMC` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '店铺名称',
  `C_WEIGHT` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '毛重',
  `C_ORIGIN` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '产地',
  `C_MATERIAL` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '材质',
  `C_STYLE` int(11) NULL DEFAULT NULL COMMENT '款式',
  `N_PEOPLE` int(11) NULL DEFAULT NULL COMMENT '适用人群',
  `N_PRICE` decimal(17, 2) NULL DEFAULT NULL COMMENT '价格',
  `N_PRICE_YH` decimal(17, 2) NULL DEFAULT NULL COMMENT '优惠价',
  `N_SFCX` int(11) NULL DEFAULT NULL COMMENT '是否促销',
  `N_SFMS` int(11) NULL DEFAULT NULL COMMENT '是否秒杀',
  `C_CJR` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `DT_CJSJ` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `C_IMG` varchar(600) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片地址',
  `N_PJS` int(11) NULL DEFAULT NULL COMMENT '评价数',
  `C_GOOD_YTLX_YT` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '三级类型',
  `N_PP` int(11) NULL DEFAULT NULL COMMENT '品牌',
  `N_FG` int(11) NULL DEFAULT NULL COMMENT '风格',
  `N_CM` int(11) NULL DEFAULT NULL COMMENT '尺码',
  `C_DETAIL_IMG1` varchar(600) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '详情图片1',
  `C_DETAIL_IMG2` varchar(600) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '详情图片2',
  `C_DETAIL_IMG3` varchar(600) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '详情图片3',
  `C_DETAIL_IMG4` varchar(600) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '详情图片4',
  PRIMARY KEY (`C_ID`) USING BTREE,
  INDEX `I_PK_SHOP_GOOD`(`C_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商品信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of shop_good
-- ----------------------------
INSERT INTO `shop_good` VALUES ('10', 14, '2', '轻奢品牌假两件套装连衣裙', '葡萱旗舰店', '0.2kg', '中国大陆', '其它', NULL, 2, 159.00, 149.00, NULL, 1, NULL, NULL, 'static\\imges\\detail\\10\\0.jpg', 2000, '170', 1, 1, 1, 'static\\imges\\detail\\10\\detail\\2.jpg', 'static\\imges\\detail\\10\\detail\\3.jpg', 'static\\imges\\detail\\10\\detail\\4.jpg', 'static\\imges\\detail\\10\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('11', 14, '2', '鸭鸭服饰连衣裙2020春夏季新款', '妤遇YUUZYUUS朗戈专卖店', '0.2kg', '中国大陆', '其它', NULL, 2, 148.00, 128.00, NULL, 1, NULL, NULL, 'static\\imges\\detail\\11\\0.jpg', 3000, '170', 1, 1, 1, 'static\\imges\\detail\\11\\detail\\2.jpg', 'static\\imges\\detail\\11\\detail\\3.jpg', 'static\\imges\\detail\\11\\detail\\4.jpg', 'static\\imges\\detail\\11\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('12', 14, '2', '麦潮尚连衣裙两件套装裙小个子连衣裙', '汐颜服饰旗舰店', '0.2kg', '中国大陆', '其它', NULL, 2, 169.00, 159.00, NULL, 1, NULL, NULL, 'static\\imges\\detail\\12\\0.jpg', 1685, '170', 1, 1, 1, 'static\\imges\\detail\\12\\detail\\2.jpg', 'static\\imges\\detail\\12\\detail\\3.jpg', 'static\\imges\\detail\\12\\detail\\4.jpg', 'static\\imges\\detail\\12\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('13', 14, '2', '女装连衣裙女棉麻两件套', '欧迪惠旗舰店', '0.2kg', '中国大陆', '其它', NULL, 2, 147.00, 137.00, NULL, 1, NULL, NULL, 'static\\imges\\detail\\13\\0.jpg', 1999, '170', 1, 1, 1, 'static\\imges\\detail\\13\\detail\\2.jpg', 'static\\imges\\detail\\13\\detail\\3.jpg', 'static\\imges\\detail\\13\\detail\\4.jpg', 'static\\imges\\detail\\13\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('14', 14, '2', '休闲套装女2020春款女装', 'FANSILANEN旗舰店', '0.2kg', '中国大陆', '其它', NULL, 2, 128.00, 108.00, NULL, 1, NULL, NULL, 'static\\imges\\detail\\14\\0.jpg', 500, '170', 1, 1, 2, 'static\\imges\\detail\\14\\detail\\2.jpg', 'static\\imges\\detail\\14\\detail\\3.jpg', 'static\\imges\\detail\\14\\detail\\4.jpg', 'static\\imges\\detail\\14\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('15', 14, '2', '雪纺连衣裙女2020春季新款', '娅素琪服饰旗舰店', '0.2kg', '中国大陆', '其它', NULL, 1, 138.00, 118.00, NULL, 1, NULL, NULL, 'static\\imges\\detail\\1\\0.jpg', 600, '170', 2, 1, 2, 'static\\imges\\detail\\1\\detail\\2.jpg', 'static\\imges\\detail\\1\\detail\\3.jpg', 'static\\imges\\detail\\1\\detail\\4.jpg', 'static\\imges\\detail\\1\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('16', 14, '2', '妤遇2020新品连衣裙女', '祖燕旗舰店', '0.2kg', '中国大陆', '其它', NULL, 1, 209.00, 199.00, NULL, 1, NULL, NULL, 'static\\imges\\detail\\2\\0.jpg', 10000, '170', 2, 1, 2, 'static\\imges\\detail\\2\\detail\\2.jpg', 'static\\imges\\detail\\2\\detail\\3.jpg', 'static\\imges\\detail\\2\\detail\\4.jpg', 'static\\imges\\detail\\2\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('17', 14, '2', '2020春装新款女装时尚套装裙', 'Hcollection旗舰店', '0.2kg', '中国大陆', '其它', NULL, 1, 199.00, 179.00, NULL, 1, NULL, NULL, 'static\\imges\\detail\\3\\0.jpg', 300, '170', 2, 1, 2, 'static\\imges\\detail\\3\\detail\\2.jpg', 'static\\imges\\detail\\3\\detail\\3.jpg', 'static\\imges\\detail\\3\\detail\\4.jpg', 'static\\imges\\detail\\3\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('18', 14, '2', '欧迪惠女装粉色连衣裙女', 'Zoro服饰旗舰店', '0.2kg', '中国大陆', '其它', NULL, 1, 178.00, 159.00, NULL, NULL, NULL, NULL, 'static\\imges\\detail\\4\\0.jpg', 400, '170', 2, 1, 2, 'static\\imges\\detail\\4\\detail\\2.jpg', 'static\\imges\\detail\\4\\detail\\3.jpg', 'static\\imges\\detail\\4\\detail\\4.jpg', 'static\\imges\\detail\\4\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('19', 13, '2', '依黛琳连衣裙小清新套装裙', '葡萱旗舰店', '0.2kg', '中国大陆', '其它', NULL, 1, 169.00, 149.00, NULL, 1, NULL, NULL, 'static\\imges\\detail\\5\\0.jpg', 2000, '170', 2, 1, 2, 'static\\imges\\detail\\5\\detail\\2.jpg', 'static\\imges\\detail\\5\\detail\\3.jpg', 'static\\imges\\detail\\5\\detail\\4.jpg', 'static\\imges\\detail\\5\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('2', 14, '2', '阔腿裤套装女2020夏季', '娅素琪服饰旗舰店', '0.2kg', '中国大陆', '其它', NULL, 1, 169.00, 159.00, 1, NULL, NULL, NULL, 'static\\imges\\detail\\2\\0.jpg', 3000, '170', 1, 1, 1, 'static\\imges\\detail\\2\\detail\\2.jpg', 'static\\imges\\detail\\2\\detail\\3.jpg', 'static\\imges\\detail\\2\\detail\\4.jpg', 'static\\imges\\detail\\2\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('20', 13, '2', '胡氏佳人中长款连衣裙套装', '鸭鸭服饰女装旗舰店', '0.2kg', '中国大陆', '其它', NULL, 1, 159.00, 149.00, NULL, NULL, NULL, NULL, 'static\\imges\\detail\\6\\0.jpg', 3000, '170', 2, 1, 2, 'static\\imges\\detail\\6\\detail\\2.jpg', 'static\\imges\\detail\\6\\detail\\3.jpg', 'static\\imges\\detail\\6\\detail\\4.jpg', 'static\\imges\\detail\\6\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('21', 13, '2', '新款女装印花连衣裙', '慕颖女装专营店', '0.2kg', '中国大陆', '其它', NULL, 1, 148.00, 128.00, NULL, NULL, NULL, NULL, 'static\\imges\\detail\\7\\0.jpg', 1685, '170', 2, 1, 2, 'static\\imges\\detail\\7\\detail\\2.jpg', 'static\\imges\\detail\\7\\detail\\3.jpg', 'static\\imges\\detail\\7\\detail\\4.jpg', 'static\\imges\\detail\\7\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('22', 13, '2', '2020春夏新款裙子小香风套装女', '薇薇麦诗旗舰店', '0.2kg', '中国大陆', '其它', NULL, 1, 169.00, 159.00, NULL, NULL, NULL, NULL, 'static\\imges\\detail\\8\\0.jpg', 1999, '170', 2, 1, 2, 'static\\imges\\detail\\8\\detail\\2.jpg', 'static\\imges\\detail\\8\\detail\\3.jpg', 'static\\imges\\detail\\8\\detail\\4.jpg', 'static\\imges\\detail\\8\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('23', 13, '2', '2020夏季新款女装短袖雪纺衫', '葡萱旗舰店', '0.2kg', '中国大陆', '其它', NULL, 1, 147.00, 137.00, NULL, NULL, NULL, NULL, 'static\\imges\\detail\\9\\0.jpg', 500, '170', 2, 1, 2, 'static\\imges\\detail\\9\\detail\\2.jpg', 'static\\imges\\detail\\9\\detail\\3.jpg', 'static\\imges\\detail\\9\\detail\\4.jpg', 'static\\imges\\detail\\9\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('24', 13, '2', '春秋装新款气质通勤套装', '妤遇YUUZYUUS朗戈专卖店', '0.2kg', '中国大陆', '其它', NULL, 1, 128.00, 108.00, NULL, NULL, NULL, NULL, 'static\\imges\\detail\\10\\0.jpg', 600, '170', 2, 1, 2, 'static\\imges\\detail\\10\\detail\\2.jpg', 'static\\imges\\detail\\10\\detail\\3.jpg', 'static\\imges\\detail\\10\\detail\\4.jpg', 'static\\imges\\detail\\10\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('25', 13, '2', '妮曼琪2019秋冬季新款韩版', '汐颜服饰旗舰店', '0.2kg', '中国大陆', '其它', NULL, 1, 138.00, 118.00, NULL, NULL, NULL, NULL, 'static\\imges\\detail\\11\\0.jpg', 10000, '170', 2, 1, 2, 'static\\imges\\detail\\11\\detail\\2.jpg', 'static\\imges\\detail\\11\\detail\\3.jpg', 'static\\imges\\detail\\11\\detail\\4.jpg', 'static\\imges\\detail\\11\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('26', 13, '2', '连衣裙女装2020春夏季新款', '欧迪惠旗舰店', '0.2kg', '中国大陆', '其它', NULL, 1, 209.00, 199.00, NULL, NULL, NULL, NULL, 'static\\imges\\detail\\12\\0.jpg', 300, '170', 2, 1, 2, 'static\\imges\\detail\\12\\detail\\2.jpg', 'static\\imges\\detail\\12\\detail\\3.jpg', 'static\\imges\\detail\\12\\detail\\4.jpg', 'static\\imges\\detail\\12\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('27', 13, '2', '顾森 包臀鱼尾长袖连衣裙', 'FANSILANEN旗舰店', '0.2kg', '中国大陆', '其它', NULL, 1, 199.00, 179.00, NULL, NULL, NULL, NULL, 'static\\imges\\detail\\13\\0.jpg', 400, '170', 2, 1, 2, 'static\\imges\\detail\\13\\detail\\2.jpg', 'static\\imges\\detail\\13\\detail\\3.jpg', 'static\\imges\\detail\\13\\detail\\4.jpg', 'static\\imges\\detail\\13\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('28', 13, '2', '阔腿裤套装女2020夏季', '娅素琪服饰旗舰店', '0.2kg', '中国大陆', '其它', NULL, 1, 178.00, 159.00, NULL, NULL, NULL, NULL, 'static\\imges\\detail\\14\\0.jpg', 2000, '170', 2, 1, 2, 'static\\imges\\detail\\14\\detail\\2.jpg', 'static\\imges\\detail\\14\\detail\\3.jpg', 'static\\imges\\detail\\14\\detail\\4.jpg', 'static\\imges\\detail\\14\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('29', 13, '2', '时尚套装女2020夏季', '祖燕旗舰店', '0.2kg', '中国大陆', '其它', NULL, 1, 169.00, 149.00, NULL, NULL, NULL, NULL, 'static\\imges\\detail\\1\\0.jpg', 3000, '170', 2, 1, 2, 'static\\imges\\detail\\1\\detail\\2.jpg', 'static\\imges\\detail\\1\\detail\\3.jpg', 'static\\imges\\detail\\1\\detail\\4.jpg', 'static\\imges\\detail\\1\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('3', 14, '2', '时尚套装女2020夏季', '祖燕旗舰店', '0.2kg', '中国大陆', '其它', NULL, 1, 147.00, 137.00, 1, NULL, NULL, NULL, 'static\\imges\\detail\\3\\0.jpg', 1685, '170', 1, 1, 1, 'static\\imges\\detail\\3\\detail\\2.jpg', 'static\\imges\\detail\\3\\detail\\3.jpg', 'static\\imges\\detail\\3\\detail\\4.jpg', 'static\\imges\\detail\\3\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('30', 1, '2', '加肥加大码女装2020', 'Hcollection旗舰店', '0.2kg', '中国大陆', '其它', NULL, 1, 159.00, 149.00, NULL, NULL, NULL, NULL, 'static\\imges\\detail\\2\\0.jpg', 1685, '170', 2, 1, 2, 'static\\imges\\detail\\2\\detail\\2.jpg', 'static\\imges\\detail\\2\\detail\\3.jpg', 'static\\imges\\detail\\2\\detail\\4.jpg', 'static\\imges\\detail\\2\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('31', 1, '2', '2020春夏新款连衣裙套装', 'Zoro服饰旗舰店', '0.2kg', '中国大陆', '其它', NULL, 1, 148.00, 128.00, NULL, NULL, NULL, NULL, 'static\\imges\\detail\\3\\0.jpg', 1999, '170', 2, 1, 3, 'static\\imges\\detail\\3\\detail\\2.jpg', 'static\\imges\\detail\\3\\detail\\3.jpg', 'static\\imges\\detail\\3\\detail\\4.jpg', 'static\\imges\\detail\\3\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('32', 1, '2', '轻奢品牌真丝连衣裙', '葡萱旗舰店', '0.2kg', '中国大陆', '其它', NULL, 1, 169.00, 159.00, NULL, NULL, NULL, NULL, 'static\\imges\\detail\\4\\0.jpg', 500, '170', 2, 1, 3, 'static\\imges\\detail\\4\\detail\\2.jpg', 'static\\imges\\detail\\4\\detail\\3.jpg', 'static\\imges\\detail\\4\\detail\\4.jpg', 'static\\imges\\detail\\4\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('33', 1, '2', '2020春冬季新款女气质针织裙', '鸭鸭服饰女装旗舰店', '0.2kg', '中国大陆', '其它', NULL, 1, 147.00, 137.00, NULL, NULL, NULL, NULL, 'static\\imges\\detail\\5\\0.jpg', 600, '170', 2, 1, 3, 'static\\imges\\detail\\5\\detail\\2.jpg', 'static\\imges\\detail\\5\\detail\\3.jpg', 'static\\imges\\detail\\5\\detail\\4.jpg', 'static\\imges\\detail\\5\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('34', 1, '2', '夏季新款流行女装', '慕颖女装专营店', '0.2kg', '中国大陆', '其它', NULL, 1, 128.00, 108.00, NULL, NULL, NULL, NULL, 'static\\imges\\detail\\6\\0.jpg', 10000, '170', 3, 1, 3, 'static\\imges\\detail\\6\\detail\\2.jpg', 'static\\imges\\detail\\6\\detail\\3.jpg', 'static\\imges\\detail\\6\\detail\\4.jpg', 'static\\imges\\detail\\6\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('35', 1, '2', '2020女装春装新品欧美淑女', '薇薇麦诗旗舰店', '0.2kg', '中国大陆', '其它', NULL, 1, 138.00, 118.00, NULL, NULL, NULL, NULL, 'static\\imges\\detail\\7\\0.jpg', 300, '170', 3, 1, 3, 'static\\imges\\detail\\7\\detail\\2.jpg', 'static\\imges\\detail\\7\\detail\\3.jpg', 'static\\imges\\detail\\7\\detail\\4.jpg', 'static\\imges\\detail\\7\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('36', 1, '2', '轻奢品牌假两件套装连衣裙', '葡萱旗舰店', '0.2kg', '中国大陆', '其它', NULL, 1, 209.00, 199.00, NULL, NULL, NULL, NULL, 'static\\imges\\detail\\8\\0.jpg', 400, '170', 3, 1, 3, 'static\\imges\\detail\\8\\detail\\2.jpg', 'static\\imges\\detail\\8\\detail\\3.jpg', 'static\\imges\\detail\\8\\detail\\4.jpg', 'static\\imges\\detail\\8\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('37', 1, '2', '鸭鸭服饰连衣裙2020春夏季新款', '妤遇YUUZYUUS朗戈专卖店', '0.2kg', '中国大陆', '其它', NULL, 1, 199.00, 179.00, NULL, NULL, NULL, NULL, 'static\\imges\\detail\\9\\0.jpg', 2000, '170', 3, 1, 3, 'static\\imges\\detail\\9\\detail\\2.jpg', 'static\\imges\\detail\\9\\detail\\3.jpg', 'static\\imges\\detail\\9\\detail\\4.jpg', 'static\\imges\\detail\\9\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('38', 1, '2', '麦潮尚连衣裙两件套装裙小个子连衣裙', '汐颜服饰旗舰店', '0.2kg', '中国大陆', '其它', NULL, 1, 178.00, 159.00, NULL, NULL, NULL, NULL, 'static\\imges\\detail\\10\\0.jpg', 3000, '170', 3, 1, 3, 'static\\imges\\detail\\10\\detail\\2.jpg', 'static\\imges\\detail\\10\\detail\\3.jpg', 'static\\imges\\detail\\10\\detail\\4.jpg', 'static\\imges\\detail\\10\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('39', 1, '2', '女装连衣裙女棉麻两件套', '欧迪惠旗舰店', '0.2kg', '中国大陆', '其它', NULL, 1, 169.00, 149.00, NULL, NULL, NULL, NULL, 'static\\imges\\detail\\11\\0.jpg', 1685, '173', 3, 1, 3, 'static\\imges\\detail\\11\\detail\\2.jpg', 'static\\imges\\detail\\11\\detail\\3.jpg', 'static\\imges\\detail\\11\\detail\\4.jpg', 'static\\imges\\detail\\11\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('4', 14, '2', '加肥加大码女装2020', 'Hcollection旗舰店', '0.2kg', '中国大陆', '其它', NULL, 1, 128.00, 108.00, 1, NULL, NULL, NULL, 'static\\imges\\detail\\4\\0.jpg', 1999, '170', 1, 1, 1, 'static\\imges\\detail\\4\\detail\\2.jpg', 'static\\imges\\detail\\4\\detail\\3.jpg', 'static\\imges\\detail\\4\\detail\\4.jpg', 'static\\imges\\detail\\4\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('40', 1, '2', '休闲套装女2020春款女装', 'FANSILANEN旗舰店', '0.2kg', '中国大陆', '其它', NULL, 1, 159.00, 149.00, NULL, NULL, NULL, NULL, 'static\\imges\\detail\\12\\0.jpg', 1999, '173', 3, 1, 3, 'static\\imges\\detail\\12\\detail\\2.jpg', 'static\\imges\\detail\\12\\detail\\3.jpg', 'static\\imges\\detail\\12\\detail\\4.jpg', 'static\\imges\\detail\\12\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('41', 1, '2', '雪纺连衣裙女2020春季新款', '娅素琪服饰旗舰店', '0.2kg', '中国大陆', '其它', NULL, 1, 148.00, 128.00, NULL, NULL, NULL, NULL, 'static\\imges\\detail\\13\\0.jpg', 500, '173', 3, 1, 3, 'static\\imges\\detail\\13\\detail\\2.jpg', 'static\\imges\\detail\\13\\detail\\3.jpg', 'static\\imges\\detail\\13\\detail\\4.jpg', 'static\\imges\\detail\\13\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('42', 1, '2', '妤遇2020新品连衣裙女', '祖燕旗舰店', '0.2kg', '中国大陆', '其它', NULL, 1, 169.00, 159.00, NULL, NULL, NULL, NULL, 'static\\imges\\detail\\14\\0.jpg', 600, '173', 3, 1, 3, 'static\\imges\\detail\\14\\detail\\2.jpg', 'static\\imges\\detail\\14\\detail\\3.jpg', 'static\\imges\\detail\\14\\detail\\4.jpg', 'static\\imges\\detail\\14\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('43', 1, '2', '2020春装新款女装时尚套装裙', 'Hcollection旗舰店', '0.2kg', '中国大陆', '其它', NULL, 1, 147.00, 137.00, NULL, NULL, NULL, NULL, 'static\\imges\\detail\\1\\0.jpg', 10000, '173', 3, 1, 3, 'static\\imges\\detail\\1\\detail\\2.jpg', 'static\\imges\\detail\\1\\detail\\3.jpg', 'static\\imges\\detail\\1\\detail\\4.jpg', 'static\\imges\\detail\\1\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('44', 1, '2', '欧迪惠女装粉色连衣裙女', 'Zoro服饰旗舰店', '0.2kg', '中国大陆', '其它', NULL, 1, 128.00, 108.00, NULL, NULL, NULL, NULL, 'static\\imges\\detail\\2\\0.jpg', 300, '173', 3, 1, 3, 'static\\imges\\detail\\2\\detail\\2.jpg', 'static\\imges\\detail\\2\\detail\\3.jpg', 'static\\imges\\detail\\2\\detail\\4.jpg', 'static\\imges\\detail\\2\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('45', 1, '2', '依黛琳连衣裙小清新套装裙', '葡萱旗舰店', '0.2kg', '中国大陆', '其它', NULL, 1, 138.00, 118.00, NULL, NULL, NULL, NULL, 'static\\imges\\detail\\3\\0.jpg', 400, '173', 5, 1, 4, 'static\\imges\\detail\\3\\detail\\2.jpg', 'static\\imges\\detail\\3\\detail\\3.jpg', 'static\\imges\\detail\\3\\detail\\4.jpg', 'static\\imges\\detail\\3\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('46', 1, '2', '胡氏佳人中长款连衣裙套装', '鸭鸭服饰女装旗舰店', '0.2kg', '中国大陆', '其它', NULL, 1, 209.00, 199.00, NULL, NULL, NULL, NULL, 'static\\imges\\detail\\4\\0.jpg', 401, '173', 5, 1, 4, 'static\\imges\\detail\\4\\detail\\2.jpg', 'static\\imges\\detail\\4\\detail\\3.jpg', 'static\\imges\\detail\\4\\detail\\4.jpg', 'static\\imges\\detail\\4\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('47', 1, '2', '新款女装印花连衣裙', '慕颖女装专营店', '0.2kg', '中国大陆', '其它', NULL, 1, 199.00, 179.00, NULL, NULL, NULL, NULL, 'static\\imges\\detail\\5\\0.jpg', 402, '173', 5, 1, 4, 'static\\imges\\detail\\5\\detail\\2.jpg', 'static\\imges\\detail\\5\\detail\\3.jpg', 'static\\imges\\detail\\5\\detail\\4.jpg', 'static\\imges\\detail\\5\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('48', 1, '2', '2020春夏新款裙子小香风套装女', '薇薇麦诗旗舰店', '0.2kg', '中国大陆', '其它', NULL, 1, 178.00, 159.00, NULL, NULL, NULL, NULL, 'static\\imges\\detail\\6\\0.jpg', 403, '173', 5, 1, 4, 'static\\imges\\detail\\6\\detail\\2.jpg', 'static\\imges\\detail\\6\\detail\\3.jpg', 'static\\imges\\detail\\6\\detail\\4.jpg', 'static\\imges\\detail\\6\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('49', 1, '2', '2020夏季新款女装短袖雪纺衫', '葡萱旗舰店', '0.2kg', '中国大陆', '其它', NULL, 1, 169.00, 149.00, NULL, NULL, NULL, NULL, 'static\\imges\\detail\\7\\0.jpg', 404, '173', 5, 1, 4, 'static\\imges\\detail\\7\\detail\\2.jpg', 'static\\imges\\detail\\7\\detail\\3.jpg', 'static\\imges\\detail\\7\\detail\\4.jpg', 'static\\imges\\detail\\7\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('5', 14, '2', '2020春夏新款连衣裙套装', 'Zoro服饰旗舰店', '0.2kg', '中国大陆', '其它', NULL, 1, 138.00, 118.00, 1, NULL, NULL, NULL, 'static\\imges\\detail\\5\\0.jpg', 500, '170', 1, 1, 1, 'static\\imges\\detail\\5\\detail\\2.jpg', 'static\\imges\\detail\\5\\detail\\3.jpg', 'static\\imges\\detail\\5\\detail\\4.jpg', 'static\\imges\\detail\\5\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('50', 1, '2', '春秋装新款气质通勤套装', '妤遇YUUZYUUS朗戈专卖店', '0.2kg', '中国大陆', '其它', NULL, 1, 159.00, 149.00, NULL, NULL, NULL, NULL, 'static\\imges\\detail\\8\\0.jpg', 405, '173', 4, 2, 4, 'static\\imges\\detail\\8\\detail\\2.jpg', 'static\\imges\\detail\\8\\detail\\3.jpg', 'static\\imges\\detail\\8\\detail\\4.jpg', 'static\\imges\\detail\\8\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('51', 1, '2', '妮曼琪2019秋冬季新款韩版', '汐颜服饰旗舰店', '0.2kg', '中国大陆', '其它', NULL, 1, 148.00, 128.00, NULL, NULL, NULL, NULL, 'static\\imges\\detail\\9\\0.jpg', 406, '173', 4, 2, 4, 'static\\imges\\detail\\9\\detail\\2.jpg', 'static\\imges\\detail\\9\\detail\\3.jpg', 'static\\imges\\detail\\9\\detail\\4.jpg', 'static\\imges\\detail\\9\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('52', 1, '2', '连衣裙女装2020春夏季新款', '欧迪惠旗舰店', '0.2kg', '中国大陆', '其它', NULL, 1, 169.00, 159.00, NULL, NULL, NULL, NULL, 'static\\imges\\detail\\10\\0.jpg', 407, '173', 4, 2, 4, 'static\\imges\\detail\\10\\detail\\2.jpg', 'static\\imges\\detail\\10\\detail\\3.jpg', 'static\\imges\\detail\\10\\detail\\4.jpg', 'static\\imges\\detail\\10\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('53', 1, '2', '顾森 包臀鱼尾长袖连衣裙', '欧迪惠旗舰店', '0.2kg', '中国大陆', '其它', NULL, 1, 147.00, 137.00, NULL, NULL, NULL, NULL, 'static\\imges\\detail\\11\\0.jpg', 408, '173', 4, 2, 4, 'static\\imges\\detail\\11\\detail\\2.jpg', 'static\\imges\\detail\\11\\detail\\3.jpg', 'static\\imges\\detail\\11\\detail\\4.jpg', 'static\\imges\\detail\\11\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('54', 1, '2', '阔腿裤套装女2020夏季', 'FANSILANEN旗舰店', '0.2kg', '中国大陆', '其它', NULL, 1, 128.00, 108.00, NULL, NULL, NULL, NULL, 'static\\imges\\detail\\12\\0.jpg', 409, '173', 4, 2, 4, 'static\\imges\\detail\\12\\detail\\2.jpg', 'static\\imges\\detail\\12\\detail\\3.jpg', 'static\\imges\\detail\\12\\detail\\4.jpg', 'static\\imges\\detail\\12\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('55', 1, '2', '时尚套装女2020夏季', '娅素琪服饰旗舰店', '0.2kg', '中国大陆', '其它', NULL, 1, 138.00, 118.00, NULL, NULL, NULL, NULL, 'static\\imges\\detail\\13\\0.jpg', 410, '173', 4, 2, 4, 'static\\imges\\detail\\13\\detail\\2.jpg', 'static\\imges\\detail\\13\\detail\\3.jpg', 'static\\imges\\detail\\13\\detail\\4.jpg', 'static\\imges\\detail\\13\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('56', 1, '2', '加肥加大码女装2020', '祖燕旗舰店', '0.2kg', '中国大陆', '其它', NULL, 1, 209.00, 199.00, NULL, NULL, NULL, NULL, 'static\\imges\\detail\\14\\0.jpg', 411, '173', 4, 2, 4, 'static\\imges\\detail\\14\\detail\\2.jpg', 'static\\imges\\detail\\14\\detail\\3.jpg', 'static\\imges\\detail\\14\\detail\\4.jpg', 'static\\imges\\detail\\14\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('57', 1, '2', '2020春夏新款连衣裙套装', 'Hcollection旗舰店', '0.2kg', '中国大陆', '其它', NULL, 1, 199.00, 179.00, NULL, NULL, NULL, NULL, 'static\\imges\\detail\\1\\0.jpg', 412, '173', 1, 2, 4, 'static\\imges\\detail\\1\\detail\\2.jpg', 'static\\imges\\detail\\1\\detail\\3.jpg', 'static\\imges\\detail\\1\\detail\\4.jpg', 'static\\imges\\detail\\1\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('58', 1, '2', '轻奢品牌真丝连衣裙', 'Zoro服饰旗舰店', '0.2kg', '中国大陆', '其它', NULL, 1, 178.00, 159.00, NULL, NULL, NULL, NULL, 'static\\imges\\detail\\2\\0.jpg', 413, '173', 1, 2, 4, 'static\\imges\\detail\\2\\detail\\2.jpg', 'static\\imges\\detail\\2\\detail\\3.jpg', 'static\\imges\\detail\\2\\detail\\4.jpg', 'static\\imges\\detail\\2\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('59', 1, '2', '2020春冬季新款女气质针织裙', '葡萱旗舰店', '0.2kg', '中国大陆', '其它', NULL, 1, 169.00, 149.00, NULL, NULL, NULL, NULL, 'static\\imges\\detail\\3\\0.jpg', 414, '173', 1, 2, 4, 'static\\imges\\detail\\3\\detail\\2.jpg', 'static\\imges\\detail\\3\\detail\\3.jpg', 'static\\imges\\detail\\3\\detail\\4.jpg', 'static\\imges\\detail\\3\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('6', 14, '2', '轻奢品牌真丝连衣裙', '葡萱旗舰店', '0.2kg', '中国大陆', '其它', NULL, 1, 209.00, 199.00, 1, NULL, NULL, NULL, 'static\\imges\\detail\\6\\0.jpg', 600, '170', 1, 1, 1, 'static\\imges\\detail\\6\\detail\\2.jpg', 'static\\imges\\detail\\6\\detail\\3.jpg', 'static\\imges\\detail\\6\\detail\\4.jpg', 'static\\imges\\detail\\6\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('60', 1, '2', '夏季新款流行女装', '鸭鸭服饰女装旗舰店', '0.2kg', '中国大陆', '其它', NULL, 1, 159.00, 149.00, NULL, NULL, NULL, NULL, 'static\\imges\\detail\\4\\0.jpg', 415, '173', 1, 2, 4, 'static\\imges\\detail\\4\\detail\\2.jpg', 'static\\imges\\detail\\4\\detail\\3.jpg', 'static\\imges\\detail\\4\\detail\\4.jpg', 'static\\imges\\detail\\4\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('61', 1, '2', '2020女装春装新品欧美淑女', '慕颖女装专营店', '0.2kg', '中国大陆', '其它', NULL, 1, 148.00, 128.00, NULL, NULL, NULL, NULL, 'static\\imges\\detail\\5\\0.jpg', 416, '173', 1, 2, 4, 'static\\imges\\detail\\5\\detail\\2.jpg', 'static\\imges\\detail\\5\\detail\\3.jpg', 'static\\imges\\detail\\5\\detail\\4.jpg', 'static\\imges\\detail\\5\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('62', 1, '2', '轻奢品牌假两件套装连衣裙', '薇薇麦诗旗舰店', '0.2kg', '中国大陆', '其它', NULL, 1, 169.00, 159.00, NULL, NULL, NULL, NULL, 'static\\imges\\detail\\6\\0.jpg', 417, '173', 1, 2, 4, 'static\\imges\\detail\\6\\detail\\2.jpg', 'static\\imges\\detail\\6\\detail\\3.jpg', 'static\\imges\\detail\\6\\detail\\4.jpg', 'static\\imges\\detail\\6\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('63', 1, '2', '鸭鸭服饰连衣裙2020春夏季新款', '葡萱旗舰店', '0.2kg', '中国大陆', '其它', NULL, 1, 147.00, 137.00, NULL, NULL, NULL, NULL, 'static\\imges\\detail\\7\\0.jpg', 418, '173', 1, 2, 5, 'static\\imges\\detail\\7\\detail\\2.jpg', 'static\\imges\\detail\\7\\detail\\3.jpg', 'static\\imges\\detail\\7\\detail\\4.jpg', 'static\\imges\\detail\\7\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('64', 1, '2', '麦潮尚连衣裙两件套装裙小个子连衣裙', '妤遇YUUZYUUS朗戈专卖店', '0.2kg', '中国大陆', '其它', NULL, 1, 128.00, 108.00, NULL, NULL, NULL, NULL, 'static\\imges\\detail\\8\\0.jpg', 419, '173', 1, 2, 5, 'static\\imges\\detail\\8\\detail\\2.jpg', 'static\\imges\\detail\\8\\detail\\3.jpg', 'static\\imges\\detail\\8\\detail\\4.jpg', 'static\\imges\\detail\\8\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('65', 1, '2', '女装连衣裙女棉麻两件套', '汐颜服饰旗舰店', '0.2kg', '中国大陆', '其它', NULL, 1, 138.00, 118.00, NULL, NULL, NULL, NULL, 'static\\imges\\detail\\9\\0.jpg', 420, '173', 1, 2, 5, 'static\\imges\\detail\\9\\detail\\2.jpg', 'static\\imges\\detail\\9\\detail\\3.jpg', 'static\\imges\\detail\\9\\detail\\4.jpg', 'static\\imges\\detail\\9\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('66', 1, '2', '休闲套装女2020春款女装', '欧迪惠旗舰店', '0.2kg', '中国大陆', '其它', NULL, 1, 209.00, 199.00, NULL, NULL, NULL, NULL, 'static\\imges\\detail\\10\\0.jpg', 421, '173', 1, 2, 5, 'static\\imges\\detail\\10\\detail\\2.jpg', 'static\\imges\\detail\\10\\detail\\3.jpg', 'static\\imges\\detail\\10\\detail\\4.jpg', 'static\\imges\\detail\\10\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('67', 1, '2', '雪纺连衣裙女2020春季新款', 'FANSILANEN旗舰店', '0.2kg', '中国大陆', '其它', NULL, 1, 199.00, 179.00, NULL, NULL, NULL, NULL, 'static\\imges\\detail\\11\\0.jpg', 422, '173', 1, 2, 5, 'static\\imges\\detail\\11\\detail\\2.jpg', 'static\\imges\\detail\\11\\detail\\3.jpg', 'static\\imges\\detail\\11\\detail\\4.jpg', 'static\\imges\\detail\\11\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('68', 1, '2', '妤遇2020新品连衣裙女', '娅素琪服饰旗舰店', '0.2kg', '中国大陆', '其它', NULL, 1, 178.00, 159.00, NULL, NULL, NULL, NULL, 'static\\imges\\detail\\12\\0.jpg', 423, '173', 1, 2, 5, 'static\\imges\\detail\\12\\detail\\2.jpg', 'static\\imges\\detail\\12\\detail\\3.jpg', 'static\\imges\\detail\\12\\detail\\4.jpg', 'static\\imges\\detail\\12\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('69', 1, '2', '2020春装新款女装时尚套装裙', '祖燕旗舰店', '0.2kg', '中国大陆', '其它', NULL, 1, 169.00, 149.00, NULL, NULL, NULL, NULL, 'static\\imges\\detail\\13\\0.jpg', 424, '173', 1, 2, 5, 'static\\imges\\detail\\13\\detail\\2.jpg', 'static\\imges\\detail\\13\\detail\\3.jpg', 'static\\imges\\detail\\13\\detail\\4.jpg', 'static\\imges\\detail\\13\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('7', 14, '2', '2020春冬季新款女气质针织裙', '鸭鸭服饰女装旗舰店', '0.2kg', '中国大陆', '其它', NULL, 1, 199.00, 179.00, 1, NULL, NULL, NULL, 'static\\imges\\detail\\7\\0.jpg', 10000, '170', 1, 1, 1, 'static\\imges\\detail\\7\\detail\\2.jpg', 'static\\imges\\detail\\7\\detail\\3.jpg', 'static\\imges\\detail\\7\\detail\\4.jpg', 'static\\imges\\detail\\7\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('70', 1, '2', '欧迪惠女装粉色连衣裙女', 'Hcollection旗舰店', '0.2kg', '中国大陆', '其它', NULL, 1, 159.00, 149.00, NULL, NULL, NULL, NULL, 'static\\imges\\detail\\14\\0.jpg', 425, '173', 1, 2, 5, 'static\\imges\\detail\\14\\detail\\2.jpg', 'static\\imges\\detail\\14\\detail\\3.jpg', 'static\\imges\\detail\\14\\detail\\4.jpg', 'static\\imges\\detail\\14\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('71', 1, '2', '依黛琳连衣裙小清新套装裙', 'Zoro服饰旗舰店', '0.2kg', '中国大陆', '其它', NULL, 1, 148.00, 128.00, NULL, NULL, NULL, NULL, 'static\\imges\\detail\\1\\0.jpg', 426, '173', 1, 2, 5, 'static\\imges\\detail\\1\\detail\\2.jpg', 'static\\imges\\detail\\1\\detail\\3.jpg', 'static\\imges\\detail\\1\\detail\\4.jpg', 'static\\imges\\detail\\1\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('72', 1, '2', '胡氏佳人中长款连衣裙套装', '葡萱旗舰店', '0.2kg', '中国大陆', '其它', NULL, 1, 169.00, 159.00, NULL, NULL, NULL, NULL, 'static\\imges\\detail\\2\\0.jpg', 427, '173', 1, 2, 5, 'static\\imges\\detail\\2\\detail\\2.jpg', 'static\\imges\\detail\\2\\detail\\3.jpg', 'static\\imges\\detail\\2\\detail\\4.jpg', 'static\\imges\\detail\\2\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('73', 1, '2', '新款女装印花连衣裙', '鸭鸭服饰女装旗舰店', '0.2kg', '中国大陆', '其它', NULL, 1, 147.00, 137.00, NULL, NULL, NULL, NULL, 'static\\imges\\detail\\3\\0.jpg', 428, '173', 1, 2, 5, 'static\\imges\\detail\\3\\detail\\2.jpg', 'static\\imges\\detail\\3\\detail\\3.jpg', 'static\\imges\\detail\\3\\detail\\4.jpg', 'static\\imges\\detail\\3\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('74', 1, '2', '2020春夏新款裙子小香风套装女', '慕颖女装专营店', '0.2kg', '中国大陆', '其它', NULL, 1, 128.00, 108.00, NULL, NULL, NULL, NULL, 'static\\imges\\detail\\4\\0.jpg', 429, '173', 1, 2, 5, 'static\\imges\\detail\\4\\detail\\2.jpg', 'static\\imges\\detail\\4\\detail\\3.jpg', 'static\\imges\\detail\\4\\detail\\4.jpg', 'static\\imges\\detail\\4\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('75', 1, '2', '2020夏季新款女装短袖雪纺衫', '薇薇麦诗旗舰店', '0.2kg', '中国大陆', '其它', NULL, 1, 138.00, 118.00, NULL, NULL, NULL, NULL, 'static\\imges\\detail\\5\\0.jpg', 430, '173', 1, 2, 5, 'static\\imges\\detail\\5\\detail\\2.jpg', 'static\\imges\\detail\\5\\detail\\3.jpg', 'static\\imges\\detail\\5\\detail\\4.jpg', 'static\\imges\\detail\\5\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('76', 1, '2', '春秋装新款气质通勤套装', '葡萱旗舰店', '0.2kg', '中国大陆', '其它', NULL, 1, 209.00, 199.00, NULL, NULL, NULL, NULL, 'static\\imges\\detail\\6\\0.jpg', 431, '173', 1, 2, 5, 'static\\imges\\detail\\6\\detail\\2.jpg', 'static\\imges\\detail\\6\\detail\\3.jpg', 'static\\imges\\detail\\6\\detail\\4.jpg', 'static\\imges\\detail\\6\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('77', 1, '2', '妮曼琪2019秋冬季新款韩版', '妤遇YUUZYUUS朗戈专卖店', '0.2kg', '中国大陆', '其它', NULL, 1, 199.00, 179.00, NULL, NULL, NULL, NULL, 'static\\imges\\detail\\7\\0.jpg', 432, '173', 1, 2, 5, 'static\\imges\\detail\\7\\detail\\2.jpg', 'static\\imges\\detail\\7\\detail\\3.jpg', 'static\\imges\\detail\\7\\detail\\4.jpg', 'static\\imges\\detail\\7\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('78', 1, '2', '连衣裙女装2020春夏季新款', '汐颜服饰旗舰店', '0.2kg', '中国大陆', '其它', NULL, 1, 178.00, 159.00, NULL, NULL, NULL, NULL, 'static\\imges\\detail\\8\\0.jpg', 433, '173', 1, 2, 5, 'static\\imges\\detail\\8\\detail\\2.jpg', 'static\\imges\\detail\\8\\detail\\3.jpg', 'static\\imges\\detail\\8\\detail\\4.jpg', 'static\\imges\\detail\\8\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('79', 1, '2', '顾森 包臀鱼尾长袖连衣裙', '欧迪惠旗舰店', '0.2kg', '中国大陆', '其它', NULL, 1, 169.00, 149.00, NULL, NULL, NULL, NULL, 'static\\imges\\detail\\9\\0.jpg', 434, '173', 1, 2, 5, 'static\\imges\\detail\\9\\detail\\2.jpg', 'static\\imges\\detail\\9\\detail\\3.jpg', 'static\\imges\\detail\\9\\detail\\4.jpg', 'static\\imges\\detail\\9\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('8', 14, '2', '夏季新款流行女装', '慕颖女装专营店', '0.2kg', '中国大陆', '其它', NULL, 1, 178.00, 159.00, 1, NULL, NULL, NULL, 'static\\imges\\detail\\8\\0.jpg', 300, '170', 1, 1, 1, 'static\\imges\\detail\\8\\detail\\2.jpg', 'static\\imges\\detail\\8\\detail\\3.jpg', 'static\\imges\\detail\\8\\detail\\4.jpg', 'static\\imges\\detail\\8\\detail\\5.jpg');
INSERT INTO `shop_good` VALUES ('9', 14, '2', '2020女装春装新品欧美淑女', '薇薇麦诗旗舰店', '0.2kg', '中国大陆', '其它', NULL, 2, 169.00, 149.00, NULL, 1, NULL, NULL, 'static\\imges\\detail\\9\\0.jpg', 400, '170', 1, 1, 1, 'static\\imges\\detail\\9\\detail\\2.jpg', 'static\\imges\\detail\\9\\detail\\3.jpg', 'static\\imges\\detail\\9\\detail\\4.jpg', 'static\\imges\\detail\\9\\detail\\5.jpg');

-- ----------------------------
-- Table structure for shop_good_used
-- ----------------------------
DROP TABLE IF EXISTS `shop_good_used`;
CREATE TABLE `shop_good_used`  (
  `C_ID` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号',
  `N_GOOD_LX` int(11) NULL DEFAULT NULL COMMENT '商品类型',
  `C_GOOD_YT` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品用途',
  `N_LEVEL` int(11) NULL DEFAULT NULL COMMENT '等级',
  `N_ORDER` int(11) NULL DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`C_ID`) USING BTREE,
  INDEX `I_PK_SHOP_GOOD_USED`(`C_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商品用途分类表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of shop_good_used
-- ----------------------------
INSERT INTO `shop_good_used` VALUES ('1', 1, '男装', 1, 1);
INSERT INTO `shop_good_used` VALUES ('10', 4, '童鞋', 1, 10);
INSERT INTO `shop_good_used` VALUES ('100', 6, '粉底', 2, 100);
INSERT INTO `shop_good_used` VALUES ('101', 6, '美甲', 2, 101);
INSERT INTO `shop_good_used` VALUES ('102', 6, '香水', 2, 102);
INSERT INTO `shop_good_used` VALUES ('103', 6, '衬衫', 2, 103);
INSERT INTO `shop_good_used` VALUES ('104', 6, '西服', 2, 104);
INSERT INTO `shop_good_used` VALUES ('105', 6, '休闲', 2, 105);
INSERT INTO `shop_good_used` VALUES ('106', 6, '羽绒服', 2, 106);
INSERT INTO `shop_good_used` VALUES ('107', 6, '加绒裤', 2, 107);
INSERT INTO `shop_good_used` VALUES ('108', 6, '棉服', 2, 108);
INSERT INTO `shop_good_used` VALUES ('109', 6, '太阳镜', 2, 109);
INSERT INTO `shop_good_used` VALUES ('11', 4, '流行', 1, 11);
INSERT INTO `shop_good_used` VALUES ('110', 7, '口红', 2, 110);
INSERT INTO `shop_good_used` VALUES ('111', 7, '粉底', 2, 111);
INSERT INTO `shop_good_used` VALUES ('112', 7, '美甲', 2, 112);
INSERT INTO `shop_good_used` VALUES ('113', 7, '香水', 2, 113);
INSERT INTO `shop_good_used` VALUES ('114', 7, '衬衫', 2, 114);
INSERT INTO `shop_good_used` VALUES ('115', 7, '西服', 2, 115);
INSERT INTO `shop_good_used` VALUES ('116', 7, '休闲', 2, 116);
INSERT INTO `shop_good_used` VALUES ('117', 7, '羽绒服', 2, 117);
INSERT INTO `shop_good_used` VALUES ('118', 7, '男装', 2, 118);
INSERT INTO `shop_good_used` VALUES ('119', 7, '女装', 2, 119);
INSERT INTO `shop_good_used` VALUES ('12', 4, '运动', 1, 12);
INSERT INTO `shop_good_used` VALUES ('120', 7, '男鞋', 2, 120);
INSERT INTO `shop_good_used` VALUES ('121', 8, '女鞋', 2, 121);
INSERT INTO `shop_good_used` VALUES ('122', 8, '内衣配饰', 2, 122);
INSERT INTO `shop_good_used` VALUES ('123', 8, '箱包手袋', 2, 123);
INSERT INTO `shop_good_used` VALUES ('124', 8, '美妆护肤', 2, 124);
INSERT INTO `shop_good_used` VALUES ('125', 8, '个护清洁', 2, 125);
INSERT INTO `shop_good_used` VALUES ('126', 8, '童装', 2, 126);
INSERT INTO `shop_good_used` VALUES ('127', 8, '童鞋', 2, 127);
INSERT INTO `shop_good_used` VALUES ('128', 8, '流行', 2, 128);
INSERT INTO `shop_good_used` VALUES ('129', 8, '运动', 2, 129);
INSERT INTO `shop_good_used` VALUES ('13', 5, '衬衫', 1, 13);
INSERT INTO `shop_good_used` VALUES ('130', 8, '衬衫', 2, 130);
INSERT INTO `shop_good_used` VALUES ('131', 8, '西服', 2, 131);
INSERT INTO `shop_good_used` VALUES ('132', 8, '休闲', 2, 132);
INSERT INTO `shop_good_used` VALUES ('133', 9, '羽绒服', 2, 133);
INSERT INTO `shop_good_used` VALUES ('134', 9, '加绒裤', 2, 134);
INSERT INTO `shop_good_used` VALUES ('135', 9, '棉服', 2, 135);
INSERT INTO `shop_good_used` VALUES ('136', 9, '太阳镜', 2, 136);
INSERT INTO `shop_good_used` VALUES ('137', 9, '口红', 2, 137);
INSERT INTO `shop_good_used` VALUES ('138', 9, '粉底', 2, 138);
INSERT INTO `shop_good_used` VALUES ('139', 9, '美甲', 2, 139);
INSERT INTO `shop_good_used` VALUES ('14', 5, '西服', 1, 14);
INSERT INTO `shop_good_used` VALUES ('140', 9, '香水', 2, 140);
INSERT INTO `shop_good_used` VALUES ('141', 9, '衬衫', 2, 141);
INSERT INTO `shop_good_used` VALUES ('142', 9, '西服', 2, 142);
INSERT INTO `shop_good_used` VALUES ('143', 9, '休闲', 2, 143);
INSERT INTO `shop_good_used` VALUES ('144', 9, '羽绒服', 2, 144);
INSERT INTO `shop_good_used` VALUES ('145', 10, '加绒裤', 2, 145);
INSERT INTO `shop_good_used` VALUES ('146', 10, '棉服', 2, 146);
INSERT INTO `shop_good_used` VALUES ('147', 10, '太阳镜', 2, 147);
INSERT INTO `shop_good_used` VALUES ('148', 10, '口红', 2, 148);
INSERT INTO `shop_good_used` VALUES ('149', 10, '粉底', 2, 149);
INSERT INTO `shop_good_used` VALUES ('15', 5, '休闲', 1, 15);
INSERT INTO `shop_good_used` VALUES ('150', 10, '美甲', 2, 150);
INSERT INTO `shop_good_used` VALUES ('151', 10, '香水', 2, 151);
INSERT INTO `shop_good_used` VALUES ('152', 10, '衬衫', 2, 152);
INSERT INTO `shop_good_used` VALUES ('153', 10, '西服', 2, 153);
INSERT INTO `shop_good_used` VALUES ('154', 10, '休闲', 2, 154);
INSERT INTO `shop_good_used` VALUES ('155', 10, '羽绒服', 2, 155);
INSERT INTO `shop_good_used` VALUES ('156', 10, '男装', 2, 156);
INSERT INTO `shop_good_used` VALUES ('157', 11, '女装', 2, 157);
INSERT INTO `shop_good_used` VALUES ('158', 11, '男鞋', 2, 158);
INSERT INTO `shop_good_used` VALUES ('159', 11, '女鞋', 2, 159);
INSERT INTO `shop_good_used` VALUES ('16', 6, '羽绒服', 1, 16);
INSERT INTO `shop_good_used` VALUES ('160', 11, '内衣配饰', 2, 160);
INSERT INTO `shop_good_used` VALUES ('161', 11, '箱包手袋', 2, 161);
INSERT INTO `shop_good_used` VALUES ('162', 11, '美妆护肤', 2, 162);
INSERT INTO `shop_good_used` VALUES ('163', 11, '个护清洁', 2, 163);
INSERT INTO `shop_good_used` VALUES ('164', 11, '童装', 2, 164);
INSERT INTO `shop_good_used` VALUES ('165', 11, '童鞋', 2, 165);
INSERT INTO `shop_good_used` VALUES ('166', 11, '流行', 2, 166);
INSERT INTO `shop_good_used` VALUES ('167', 11, '运动', 2, 167);
INSERT INTO `shop_good_used` VALUES ('168', 11, '衬衫', 2, 168);
INSERT INTO `shop_good_used` VALUES ('169', 42, '男装', 3, 169);
INSERT INTO `shop_good_used` VALUES ('17', 6, '加绒裤', 1, 17);
INSERT INTO `shop_good_used` VALUES ('170', 42, '女装', 3, 170);
INSERT INTO `shop_good_used` VALUES ('171', 42, '男鞋', 3, 171);
INSERT INTO `shop_good_used` VALUES ('172', 42, '女鞋', 3, 172);
INSERT INTO `shop_good_used` VALUES ('173', 42, '内衣配饰', 3, 173);
INSERT INTO `shop_good_used` VALUES ('174', 42, '箱包手袋', 3, 174);
INSERT INTO `shop_good_used` VALUES ('175', 42, '美妆护肤', 3, 175);
INSERT INTO `shop_good_used` VALUES ('176', 42, '个护清洁', 3, 176);
INSERT INTO `shop_good_used` VALUES ('177', 42, '童装', 3, 177);
INSERT INTO `shop_good_used` VALUES ('178', 42, '童鞋', 3, 178);
INSERT INTO `shop_good_used` VALUES ('179', 42, '流行', 3, 179);
INSERT INTO `shop_good_used` VALUES ('18', 6, '棉服', 1, 18);
INSERT INTO `shop_good_used` VALUES ('180', 43, '运动', 3, 180);
INSERT INTO `shop_good_used` VALUES ('181', 43, '衬衫', 3, 181);
INSERT INTO `shop_good_used` VALUES ('182', 43, '西服', 3, 182);
INSERT INTO `shop_good_used` VALUES ('183', 43, '休闲', 3, 183);
INSERT INTO `shop_good_used` VALUES ('184', 43, '羽绒服', 3, 184);
INSERT INTO `shop_good_used` VALUES ('185', 43, '加绒裤', 3, 185);
INSERT INTO `shop_good_used` VALUES ('186', 43, '棉服', 3, 186);
INSERT INTO `shop_good_used` VALUES ('187', 43, '太阳镜', 3, 187);
INSERT INTO `shop_good_used` VALUES ('188', 43, '口红', 3, 188);
INSERT INTO `shop_good_used` VALUES ('189', 43, '粉底', 3, 189);
INSERT INTO `shop_good_used` VALUES ('19', 7, '太阳镜', 1, 19);
INSERT INTO `shop_good_used` VALUES ('190', 43, '美甲', 3, 190);
INSERT INTO `shop_good_used` VALUES ('191', 43, '香水', 3, 191);
INSERT INTO `shop_good_used` VALUES ('192', 44, '衬衫', 3, 192);
INSERT INTO `shop_good_used` VALUES ('193', 44, '西服', 3, 193);
INSERT INTO `shop_good_used` VALUES ('194', 44, '休闲', 3, 194);
INSERT INTO `shop_good_used` VALUES ('195', 44, '羽绒服', 3, 195);
INSERT INTO `shop_good_used` VALUES ('196', 44, '加绒裤', 3, 196);
INSERT INTO `shop_good_used` VALUES ('197', 44, '棉服', 3, 197);
INSERT INTO `shop_good_used` VALUES ('198', 44, '太阳镜', 3, 198);
INSERT INTO `shop_good_used` VALUES ('199', 44, '口红', 3, 199);
INSERT INTO `shop_good_used` VALUES ('2', 1, '女装', 1, 2);
INSERT INTO `shop_good_used` VALUES ('20', 7, '口红', 1, 20);
INSERT INTO `shop_good_used` VALUES ('200', 44, '粉底', 3, 200);
INSERT INTO `shop_good_used` VALUES ('201', 44, '美甲', 3, 201);
INSERT INTO `shop_good_used` VALUES ('202', 45, '香水', 3, 202);
INSERT INTO `shop_good_used` VALUES ('203', 45, '衬衫', 3, 203);
INSERT INTO `shop_good_used` VALUES ('204', 45, '西服', 3, 204);
INSERT INTO `shop_good_used` VALUES ('205', 45, '休闲', 3, 205);
INSERT INTO `shop_good_used` VALUES ('206', 45, '羽绒服', 3, 206);
INSERT INTO `shop_good_used` VALUES ('207', 45, '男装', 3, 207);
INSERT INTO `shop_good_used` VALUES ('208', 45, '女装', 3, 208);
INSERT INTO `shop_good_used` VALUES ('209', 45, '男鞋', 3, 209);
INSERT INTO `shop_good_used` VALUES ('21', 7, '粉底', 1, 21);
INSERT INTO `shop_good_used` VALUES ('210', 45, '女鞋', 3, 210);
INSERT INTO `shop_good_used` VALUES ('211', 45, '内衣配饰', 3, 211);
INSERT INTO `shop_good_used` VALUES ('212', 45, '箱包手袋', 3, 212);
INSERT INTO `shop_good_used` VALUES ('213', 45, '美妆护肤', 3, 213);
INSERT INTO `shop_good_used` VALUES ('214', 45, '个护清洁', 3, 214);
INSERT INTO `shop_good_used` VALUES ('215', 45, '童装', 3, 215);
INSERT INTO `shop_good_used` VALUES ('216', 46, '童鞋', 3, 216);
INSERT INTO `shop_good_used` VALUES ('217', 46, '流行', 3, 217);
INSERT INTO `shop_good_used` VALUES ('218', 46, '运动', 3, 218);
INSERT INTO `shop_good_used` VALUES ('219', 46, '衬衫', 3, 219);
INSERT INTO `shop_good_used` VALUES ('22', 8, '美甲', 1, 22);
INSERT INTO `shop_good_used` VALUES ('220', 46, '西服', 3, 220);
INSERT INTO `shop_good_used` VALUES ('221', 46, '休闲', 3, 221);
INSERT INTO `shop_good_used` VALUES ('222', 46, '羽绒服', 3, 222);
INSERT INTO `shop_good_used` VALUES ('223', 46, '加绒裤', 3, 223);
INSERT INTO `shop_good_used` VALUES ('224', 46, '棉服', 3, 224);
INSERT INTO `shop_good_used` VALUES ('225', 46, '太阳镜', 3, 225);
INSERT INTO `shop_good_used` VALUES ('226', 46, '口红', 3, 226);
INSERT INTO `shop_good_used` VALUES ('227', 46, '粉底', 3, 227);
INSERT INTO `shop_good_used` VALUES ('228', 46, '美甲', 3, 228);
INSERT INTO `shop_good_used` VALUES ('229', 47, '香水', 3, 229);
INSERT INTO `shop_good_used` VALUES ('23', 8, '香水', 1, 23);
INSERT INTO `shop_good_used` VALUES ('230', 47, '衬衫', 3, 230);
INSERT INTO `shop_good_used` VALUES ('231', 47, '西服', 3, 231);
INSERT INTO `shop_good_used` VALUES ('232', 47, '休闲', 3, 232);
INSERT INTO `shop_good_used` VALUES ('233', 47, '羽绒服', 3, 233);
INSERT INTO `shop_good_used` VALUES ('234', 47, '加绒裤', 3, 234);
INSERT INTO `shop_good_used` VALUES ('235', 47, '棉服', 3, 235);
INSERT INTO `shop_good_used` VALUES ('236', 47, '太阳镜', 3, 236);
INSERT INTO `shop_good_used` VALUES ('237', 47, '口红', 3, 237);
INSERT INTO `shop_good_used` VALUES ('238', 47, '粉底', 3, 238);
INSERT INTO `shop_good_used` VALUES ('239', 47, '美甲', 3, 239);
INSERT INTO `shop_good_used` VALUES ('24', 8, '衬衫', 1, 24);
INSERT INTO `shop_good_used` VALUES ('240', 48, '香水', 3, 240);
INSERT INTO `shop_good_used` VALUES ('241', 48, '衬衫', 3, 241);
INSERT INTO `shop_good_used` VALUES ('242', 48, '西服', 3, 242);
INSERT INTO `shop_good_used` VALUES ('243', 48, '休闲', 3, 243);
INSERT INTO `shop_good_used` VALUES ('244', 48, '羽绒服', 3, 244);
INSERT INTO `shop_good_used` VALUES ('245', 48, '男装', 3, 245);
INSERT INTO `shop_good_used` VALUES ('246', 48, '女装', 3, 246);
INSERT INTO `shop_good_used` VALUES ('247', 48, '男鞋', 3, 247);
INSERT INTO `shop_good_used` VALUES ('248', 48, '女鞋', 3, 248);
INSERT INTO `shop_good_used` VALUES ('249', 48, '内衣配饰', 3, 249);
INSERT INTO `shop_good_used` VALUES ('25', 9, '西服', 1, 25);
INSERT INTO `shop_good_used` VALUES ('250', 48, '箱包手袋', 3, 250);
INSERT INTO `shop_good_used` VALUES ('251', 49, '美妆护肤', 3, 251);
INSERT INTO `shop_good_used` VALUES ('252', 49, '个护清洁', 3, 252);
INSERT INTO `shop_good_used` VALUES ('253', 49, '童装', 3, 253);
INSERT INTO `shop_good_used` VALUES ('254', 49, '童鞋', 3, 254);
INSERT INTO `shop_good_used` VALUES ('255', 49, '流行', 3, 255);
INSERT INTO `shop_good_used` VALUES ('256', 49, '运动', 3, 256);
INSERT INTO `shop_good_used` VALUES ('257', 49, '衬衫', 3, 257);
INSERT INTO `shop_good_used` VALUES ('258', 49, '西服', 3, 258);
INSERT INTO `shop_good_used` VALUES ('259', 49, '休闲', 3, 259);
INSERT INTO `shop_good_used` VALUES ('26', 9, '休闲', 1, 26);
INSERT INTO `shop_good_used` VALUES ('260', 49, '羽绒服', 3, 260);
INSERT INTO `shop_good_used` VALUES ('261', 49, '加绒裤', 3, 261);
INSERT INTO `shop_good_used` VALUES ('262', 50, '棉服', 3, 262);
INSERT INTO `shop_good_used` VALUES ('263', 50, '太阳镜', 3, 263);
INSERT INTO `shop_good_used` VALUES ('264', 50, '口红', 3, 264);
INSERT INTO `shop_good_used` VALUES ('265', 50, '粉底', 3, 265);
INSERT INTO `shop_good_used` VALUES ('266', 50, '美甲', 3, 266);
INSERT INTO `shop_good_used` VALUES ('267', 50, '香水', 3, 267);
INSERT INTO `shop_good_used` VALUES ('268', 50, '衬衫', 3, 268);
INSERT INTO `shop_good_used` VALUES ('269', 50, '西服', 3, 269);
INSERT INTO `shop_good_used` VALUES ('27', 9, '羽绒服', 1, 27);
INSERT INTO `shop_good_used` VALUES ('270', 50, '休闲', 3, 270);
INSERT INTO `shop_good_used` VALUES ('271', 50, '羽绒服', 3, 271);
INSERT INTO `shop_good_used` VALUES ('272', 50, '加绒裤', 3, 272);
INSERT INTO `shop_good_used` VALUES ('273', 51, '棉服', 3, 273);
INSERT INTO `shop_good_used` VALUES ('274', 51, '太阳镜', 3, 274);
INSERT INTO `shop_good_used` VALUES ('275', 51, '口红', 3, 275);
INSERT INTO `shop_good_used` VALUES ('276', 51, '粉底', 3, 276);
INSERT INTO `shop_good_used` VALUES ('277', 51, '美甲', 3, 277);
INSERT INTO `shop_good_used` VALUES ('278', 51, '香水', 3, 278);
INSERT INTO `shop_good_used` VALUES ('279', 51, '衬衫', 3, 279);
INSERT INTO `shop_good_used` VALUES ('28', 10, '加绒裤', 1, 28);
INSERT INTO `shop_good_used` VALUES ('280', 51, '西服', 3, 280);
INSERT INTO `shop_good_used` VALUES ('281', 51, '休闲', 3, 281);
INSERT INTO `shop_good_used` VALUES ('282', 51, '羽绒服', 3, 282);
INSERT INTO `shop_good_used` VALUES ('283', 51, '男装', 3, 283);
INSERT INTO `shop_good_used` VALUES ('284', 52, '女装', 3, 284);
INSERT INTO `shop_good_used` VALUES ('285', 52, '男鞋', 3, 285);
INSERT INTO `shop_good_used` VALUES ('286', 52, '女鞋', 3, 286);
INSERT INTO `shop_good_used` VALUES ('287', 52, '内衣配饰', 3, 287);
INSERT INTO `shop_good_used` VALUES ('288', 52, '箱包手袋', 3, 288);
INSERT INTO `shop_good_used` VALUES ('289', 52, '美妆护肤', 3, 289);
INSERT INTO `shop_good_used` VALUES ('29', 10, '棉服', 1, 29);
INSERT INTO `shop_good_used` VALUES ('290', 52, '个护清洁', 3, 290);
INSERT INTO `shop_good_used` VALUES ('291', 52, '童装', 3, 291);
INSERT INTO `shop_good_used` VALUES ('292', 52, '童鞋', 3, 292);
INSERT INTO `shop_good_used` VALUES ('293', 52, '流行', 3, 293);
INSERT INTO `shop_good_used` VALUES ('294', 52, '运动', 3, 294);
INSERT INTO `shop_good_used` VALUES ('295', 52, '衬衫', 2, 295);
INSERT INTO `shop_good_used` VALUES ('3', 1, '男鞋', 1, 3);
INSERT INTO `shop_good_used` VALUES ('30', 10, '太阳镜', 1, 30);
INSERT INTO `shop_good_used` VALUES ('31', 11, '口红', 1, 31);
INSERT INTO `shop_good_used` VALUES ('32', 11, '粉底', 1, 32);
INSERT INTO `shop_good_used` VALUES ('33', 11, '美甲', 1, 33);
INSERT INTO `shop_good_used` VALUES ('34', 12, '香水', 1, 34);
INSERT INTO `shop_good_used` VALUES ('35', 12, '衬衫', 1, 35);
INSERT INTO `shop_good_used` VALUES ('36', 12, '西服', 1, 36);
INSERT INTO `shop_good_used` VALUES ('37', 13, '休闲', 1, 37);
INSERT INTO `shop_good_used` VALUES ('38', 13, '羽绒服', 1, 38);
INSERT INTO `shop_good_used` VALUES ('39', 13, '加绒裤', 1, 39);
INSERT INTO `shop_good_used` VALUES ('4', 2, '女鞋', 1, 4);
INSERT INTO `shop_good_used` VALUES ('40', 14, '棉服', 1, 40);
INSERT INTO `shop_good_used` VALUES ('41', 14, '太阳镜', 1, 41);
INSERT INTO `shop_good_used` VALUES ('42', 1, '男装', 2, 42);
INSERT INTO `shop_good_used` VALUES ('43', 1, '女装', 2, 43);
INSERT INTO `shop_good_used` VALUES ('44', 1, '男鞋', 2, 44);
INSERT INTO `shop_good_used` VALUES ('45', 1, '女鞋', 2, 45);
INSERT INTO `shop_good_used` VALUES ('46', 1, '内衣配饰', 2, 46);
INSERT INTO `shop_good_used` VALUES ('47', 1, '箱包手袋', 2, 47);
INSERT INTO `shop_good_used` VALUES ('48', 1, '美妆护肤', 2, 48);
INSERT INTO `shop_good_used` VALUES ('49', 1, '个护清洁', 2, 49);
INSERT INTO `shop_good_used` VALUES ('5', 2, '内衣配饰', 1, 5);
INSERT INTO `shop_good_used` VALUES ('50', 1, '童装', 2, 50);
INSERT INTO `shop_good_used` VALUES ('51', 1, '童鞋', 2, 51);
INSERT INTO `shop_good_used` VALUES ('52', 1, '流行', 2, 52);
INSERT INTO `shop_good_used` VALUES ('53', 2, '运动', 2, 53);
INSERT INTO `shop_good_used` VALUES ('54', 2, '衬衫', 2, 54);
INSERT INTO `shop_good_used` VALUES ('55', 2, '西服', 2, 55);
INSERT INTO `shop_good_used` VALUES ('56', 2, '休闲', 2, 56);
INSERT INTO `shop_good_used` VALUES ('57', 2, '羽绒服', 2, 57);
INSERT INTO `shop_good_used` VALUES ('58', 2, '加绒裤', 2, 58);
INSERT INTO `shop_good_used` VALUES ('59', 2, '棉服', 2, 59);
INSERT INTO `shop_good_used` VALUES ('6', 2, '箱包手袋', 1, 6);
INSERT INTO `shop_good_used` VALUES ('60', 2, '太阳镜', 2, 60);
INSERT INTO `shop_good_used` VALUES ('61', 2, '口红', 2, 61);
INSERT INTO `shop_good_used` VALUES ('62', 2, '粉底', 2, 62);
INSERT INTO `shop_good_used` VALUES ('63', 2, '美甲', 2, 63);
INSERT INTO `shop_good_used` VALUES ('64', 3, '香水', 2, 64);
INSERT INTO `shop_good_used` VALUES ('65', 3, '衬衫', 2, 65);
INSERT INTO `shop_good_used` VALUES ('66', 3, '西服', 2, 66);
INSERT INTO `shop_good_used` VALUES ('67', 3, '休闲', 2, 67);
INSERT INTO `shop_good_used` VALUES ('68', 3, '羽绒服', 2, 68);
INSERT INTO `shop_good_used` VALUES ('69', 3, '加绒裤', 2, 69);
INSERT INTO `shop_good_used` VALUES ('7', 3, '美妆护肤', 1, 7);
INSERT INTO `shop_good_used` VALUES ('70', 3, '棉服', 2, 70);
INSERT INTO `shop_good_used` VALUES ('71', 3, '太阳镜', 2, 71);
INSERT INTO `shop_good_used` VALUES ('72', 3, '口红', 2, 72);
INSERT INTO `shop_good_used` VALUES ('73', 3, '粉底', 2, 73);
INSERT INTO `shop_good_used` VALUES ('74', 3, '美甲', 2, 74);
INSERT INTO `shop_good_used` VALUES ('75', 4, '香水', 2, 75);
INSERT INTO `shop_good_used` VALUES ('76', 4, '衬衫', 2, 76);
INSERT INTO `shop_good_used` VALUES ('77', 4, '西服', 2, 77);
INSERT INTO `shop_good_used` VALUES ('78', 4, '休闲', 2, 78);
INSERT INTO `shop_good_used` VALUES ('79', 4, '羽绒服', 2, 79);
INSERT INTO `shop_good_used` VALUES ('8', 3, '个护清洁', 1, 8);
INSERT INTO `shop_good_used` VALUES ('80', 4, '男装', 2, 80);
INSERT INTO `shop_good_used` VALUES ('81', 4, '女装', 2, 81);
INSERT INTO `shop_good_used` VALUES ('82', 4, '男鞋', 2, 82);
INSERT INTO `shop_good_used` VALUES ('83', 4, '女鞋', 2, 83);
INSERT INTO `shop_good_used` VALUES ('84', 4, '内衣配饰', 2, 84);
INSERT INTO `shop_good_used` VALUES ('85', 4, '箱包手袋', 2, 85);
INSERT INTO `shop_good_used` VALUES ('86', 4, '美妆护肤', 2, 86);
INSERT INTO `shop_good_used` VALUES ('87', 5, '个护清洁', 2, 87);
INSERT INTO `shop_good_used` VALUES ('88', 5, '童装', 2, 88);
INSERT INTO `shop_good_used` VALUES ('89', 5, '童鞋', 2, 89);
INSERT INTO `shop_good_used` VALUES ('9', 4, '童装', 1, 9);
INSERT INTO `shop_good_used` VALUES ('90', 5, '流行', 2, 90);
INSERT INTO `shop_good_used` VALUES ('91', 5, '运动', 2, 91);
INSERT INTO `shop_good_used` VALUES ('92', 5, '衬衫', 2, 92);
INSERT INTO `shop_good_used` VALUES ('93', 5, '西服', 2, 93);
INSERT INTO `shop_good_used` VALUES ('94', 5, '休闲', 2, 94);
INSERT INTO `shop_good_used` VALUES ('95', 5, '羽绒服', 2, 95);
INSERT INTO `shop_good_used` VALUES ('96', 5, '加绒裤', 2, 96);
INSERT INTO `shop_good_used` VALUES ('97', 5, '棉服', 2, 97);
INSERT INTO `shop_good_used` VALUES ('98', 5, '太阳镜', 2, 98);
INSERT INTO `shop_good_used` VALUES ('99', 6, '口红', 2, 99);

-- ----------------------------
-- Table structure for shop_order
-- ----------------------------
DROP TABLE IF EXISTS `shop_order`;
CREATE TABLE `shop_order`  (
  `C_ID` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号',
  `C_ORDER_BH` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '订单流水号',
  `C_ADDRESS` varchar(600) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '收货地址',
  `C_BZ` varchar(600) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `C_CJR` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `DT_CJSJ` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `C_GXR` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `DT_GXSJ` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`C_ID`) USING BTREE,
  INDEX `I_PK_SHOP_ORDER`(`C_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '订单信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of shop_order
-- ----------------------------
INSERT INTO `shop_order` VALUES ('12214ff4a4304d4abc9b0cca53c73d20', '20040423320001', '测试01 广东省 广东市xxx区 燕岭路633号 130****9850 713300', '', 'jk', '2020-04-04 15:32:49', NULL, NULL);
INSERT INTO `shop_order` VALUES ('12667afd431b4387859561d2a4dac459', '20040419230001', '测试01 广东省 广东市xxx区 燕岭路633号 130****9850 713300', '111', 'jk', '2020-04-04 11:23:11', NULL, NULL);
INSERT INTO `shop_order` VALUES ('206a965fbb6b4f99b15567332c93fda9', '20041210310001', '121 广东省 广州市天河区1212 130****3210 1212', '', 'jk', '2020-04-12 02:31:19', NULL, NULL);
INSERT INTO `shop_order` VALUES ('4d4c2055d65748e49c33bca3cd7e7de1', '20040419110001', '测试01 广东省 广东市xxx区 燕岭路633号 130****9850 713300', '22', 'jk', '2020-04-04 11:11:13', NULL, NULL);
INSERT INTO `shop_order` VALUES ('68d17600602c447cb60409289f129d08', '20040419140001', '测试01 广东省 广东市xxx区 燕岭路633号 130****9850 713300', '', 'jk', '2020-04-04 11:14:45', NULL, NULL);
INSERT INTO `shop_order` VALUES ('dacfb61806f2450e933516d89312e2f5', '20040419120001', '测试01 广东省 广东市xxx区 燕岭路633号 130****9850 713300', '', 'jk', '2020-04-04 11:12:11', NULL, NULL);
INSERT INTO `shop_order` VALUES ('e8ee1c3001444241b5ac720d468470cb', '20040514060001', '请选择地址', '', 'jk', '2020-04-05 06:06:16', NULL, NULL);

-- ----------------------------
-- Table structure for shop_search
-- ----------------------------
DROP TABLE IF EXISTS `shop_search`;
CREATE TABLE `shop_search`  (
  `C_ID` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号',
  `C_SSNR` varchar(600) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '搜索内容',
  `C_CJR` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `DT_CJSJ` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`C_ID`) USING BTREE,
  INDEX `I_PK_SHOP_SEARCH`(`C_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '搜索记录表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of shop_search
-- ----------------------------
INSERT INTO `shop_search` VALUES ('1', '123', 'jk', '2020-03-31 23:17:02');
INSERT INTO `shop_search` VALUES ('2', '3333', 'jk', '2020-03-31 23:17:13');

-- ----------------------------
-- Table structure for shop_user
-- ----------------------------
DROP TABLE IF EXISTS `shop_user`;
CREATE TABLE `shop_user`  (
  `C_ID` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号',
  `C_LOGINID` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '登录名',
  `C_NAME` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `C_PASSWORD` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  `C_EMAIL` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `C_PHONE` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号码',
  `C_XMJP` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '姓名简拼',
  `N_VAILD` int(11) NULL DEFAULT NULL COMMENT '是否有效',
  `DT_CJSJ` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `DT_GXSJ` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `C_TXDZ` varchar(600) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像地址',
  PRIMARY KEY (`C_ID`) USING BTREE,
  INDEX `I_PK_SHOP_USER`(`C_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '人员表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of shop_user
-- ----------------------------
INSERT INTO `shop_user` VALUES ('031fb0ca6caa4044bcf4dc32494d3826', 'lln', NULL, 'llnlln', 'lln@163.com', '18684320248', NULL, 1, '2020-03-29 03:07:25', NULL, NULL);
INSERT INTO `shop_user` VALUES ('1', 'jk', 'jk', '123456', '1054977@qq.com', NULL, NULL, 1, '2020-03-28 15:26:58', NULL, NULL);
INSERT INTO `shop_user` VALUES ('808a699c147c4149887416082407ab33', '123', NULL, '123456', '123@qq.com', '13074136892', NULL, 1, '2020-03-28 10:25:45', NULL, NULL);
INSERT INTO `shop_user` VALUES ('874ce39320e44224b1b96745ff538fe2', 'lln', NULL, 'llnlln', 'lln@163.com', '18684320248', NULL, 1, '2020-03-29 03:07:25', NULL, NULL);

-- ----------------------------
-- Table structure for t_code
-- ----------------------------
DROP TABLE IF EXISTS `t_code`;
CREATE TABLE `t_code`  (
  `C_PID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '父级编号',
  `C_CODE` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '代码值',
  `C_NAME` varchar(600) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '代码名称',
  `C_LEVELINFO` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分级信息',
  `N_SFKWH` int(11) NULL DEFAULT NULL COMMENT '是否可维护',
  `N_VAILD` int(11) NULL DEFAULT NULL COMMENT '是否有效',
  `N_ORDER` int(11) NULL DEFAULT NULL COMMENT '序号'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '单值代码表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_code
-- ----------------------------
INSERT INTO `t_code` VALUES ('10000', '1', '是', '1', 1, 1, 1);
INSERT INTO `t_code` VALUES ('10000', '2', '否', '1', 1, 1, 2);
INSERT INTO `t_code` VALUES ('100001', '5', '美国', '1', 1, 1, 5);
INSERT INTO `t_code` VALUES ('100001', '6', '俄罗斯', '1', 1, 1, 6);
INSERT INTO `t_code` VALUES ('100001', '7', '加拿大', '1', 1, 1, 7);
INSERT INTO `t_code` VALUES ('100001', '8', '日本', '1', 1, 1, 8);
INSERT INTO `t_code` VALUES ('100001', '9', '英国', '1', 1, 1, 9);
INSERT INTO `t_code` VALUES ('100001', '10', '法国', '1', 1, 1, 10);
INSERT INTO `t_code` VALUES ('100001', '11', '德国', '1', 1, 1, 11);
INSERT INTO `t_code` VALUES ('100001', '12', '意大利', '1', 1, 1, 12);
INSERT INTO `t_code` VALUES ('100001', '13', '爱尔兰', '1', 1, 1, 13);
INSERT INTO `t_code` VALUES ('100001', '14', '荷兰', '1', 1, 1, 14);
INSERT INTO `t_code` VALUES ('100001', '15', '比利时', '1', 1, 1, 15);
INSERT INTO `t_code` VALUES ('100001', '16', '卢森堡', '1', 1, 1, 16);
INSERT INTO `t_code` VALUES ('100001', '17', '西班牙', '1', 1, 1, 17);
INSERT INTO `t_code` VALUES ('100001', '18', '葡萄牙', '1', 1, 1, 18);
INSERT INTO `t_code` VALUES ('100001', '19', '希腊', '1', 1, 1, 19);
INSERT INTO `t_code` VALUES ('100001', '20', '瑞典', '1', 1, 1, 20);
INSERT INTO `t_code` VALUES ('100001', '21', '芬兰', '1', 1, 1, 21);
INSERT INTO `t_code` VALUES ('100001', '22', '奥地利', '1', 1, 1, 22);
INSERT INTO `t_code` VALUES ('100001', '23', '丹麦', '1', 1, 1, 23);
INSERT INTO `t_code` VALUES ('100001', '24', '波兰', '1', 1, 1, 24);
INSERT INTO `t_code` VALUES ('100001', '25', '马耳他', '1', 1, 1, 25);
INSERT INTO `t_code` VALUES ('100001', '26', '斯洛文尼亚', '1', 1, 1, 26);
INSERT INTO `t_code` VALUES ('100001', '27', '匈牙利', '1', 1, 1, 27);
INSERT INTO `t_code` VALUES ('100001', '28', '立陶宛', '1', 1, 1, 28);
INSERT INTO `t_code` VALUES ('100001', '29', '斯洛伐克', '1', 1, 1, 29);
INSERT INTO `t_code` VALUES ('100001', '30', '捷克', '1', 1, 1, 30);
INSERT INTO `t_code` VALUES ('100001', '31', '爱沙尼亚', '1', 1, 1, 31);
INSERT INTO `t_code` VALUES ('100001', '32', '拉脱维亚', '1', 1, 1, 32);
INSERT INTO `t_code` VALUES ('100001', '33', '塞浦路斯', '1', 1, 1, 33);
INSERT INTO `t_code` VALUES ('100001', '34', '罗马尼亚', '1', 1, 1, 34);
INSERT INTO `t_code` VALUES ('100001', '35', '保加利亚', '1', 1, 1, 35);
INSERT INTO `t_code` VALUES ('100001', '36', '澳大利亚', '1', 1, 1, 36);
INSERT INTO `t_code` VALUES ('100001', '37', '韩国', '1', 1, 1, 37);
INSERT INTO `t_code` VALUES ('100001', '38', '印度', '1', 1, 1, 38);
INSERT INTO `t_code` VALUES ('100001', '39', '朝鲜', '1', 1, 1, 39);
INSERT INTO `t_code` VALUES ('100001', '40', '蒙古', '1', 1, 1, 40);
INSERT INTO `t_code` VALUES ('100001', '41', '老挝', '1', 1, 1, 41);
INSERT INTO `t_code` VALUES ('100001', '42', '越南', '1', 1, 1, 42);
INSERT INTO `t_code` VALUES ('100001', '43', '柬埔寨', '1', 1, 1, 43);
INSERT INTO `t_code` VALUES ('100001', '44', '缅甸', '1', 1, 1, 44);
INSERT INTO `t_code` VALUES ('100001', '45', '泰国', '1', 1, 1, 45);
INSERT INTO `t_code` VALUES ('100001', '46', '马来西亚', '1', 1, 1, 46);
INSERT INTO `t_code` VALUES ('100001', '47', '新加坡', '1', 1, 1, 47);
INSERT INTO `t_code` VALUES ('100001', '48', '文莱', '1', 1, 1, 48);
INSERT INTO `t_code` VALUES ('100001', '49', '菲律宾', '1', 1, 1, 49);
INSERT INTO `t_code` VALUES ('100001', '50', '印度尼西亚', '1', 1, 1, 50);
INSERT INTO `t_code` VALUES ('100001', '51', '东帝汶', '1', 1, 1, 51);
INSERT INTO `t_code` VALUES ('100001', '52', '尼泊尔', '1', 1, 1, 52);
INSERT INTO `t_code` VALUES ('100001', '53', '不丹', '1', 1, 1, 53);
INSERT INTO `t_code` VALUES ('100001', '54', '孟加拉', '1', 1, 1, 54);
INSERT INTO `t_code` VALUES ('100001', '55', '斯里兰卡', '1', 1, 1, 55);
INSERT INTO `t_code` VALUES ('100001', '56', '马尔代夫', '1', 1, 1, 56);
INSERT INTO `t_code` VALUES ('100001', '57', '巴基斯坦', '1', 1, 1, 57);
INSERT INTO `t_code` VALUES ('100001', '58', '阿富汗', '1', 1, 1, 58);
INSERT INTO `t_code` VALUES ('100001', '59', '伊朗', '1', 1, 1, 59);
INSERT INTO `t_code` VALUES ('100001', '60', '科威特', '1', 1, 1, 60);
INSERT INTO `t_code` VALUES ('100001', '61', '沙特阿拉伯', '1', 1, 1, 61);
INSERT INTO `t_code` VALUES ('100001', '62', '巴林', '1', 1, 1, 62);
INSERT INTO `t_code` VALUES ('100001', '63', '卡达尔', '1', 1, 1, 63);
INSERT INTO `t_code` VALUES ('100001', '64', '阿拉伯联合酋长国', '1', 1, 1, 64);
INSERT INTO `t_code` VALUES ('100001', '65', '阿曼', '1', 1, 1, 65);
INSERT INTO `t_code` VALUES ('100001', '66', '也门', '1', 1, 1, 66);
INSERT INTO `t_code` VALUES ('100001', '67', '伊拉克', '1', 1, 1, 67);
INSERT INTO `t_code` VALUES ('100001', '68', '叙利亚', '1', 1, 1, 68);
INSERT INTO `t_code` VALUES ('100001', '69', '黎巴嫩', '1', 1, 1, 69);
INSERT INTO `t_code` VALUES ('100001', '70', '约旦', '1', 1, 1, 70);
INSERT INTO `t_code` VALUES ('100001', '71', '巴勒斯坦', '1', 1, 1, 71);
INSERT INTO `t_code` VALUES ('100001', '72', '以色列', '1', 1, 1, 72);
INSERT INTO `t_code` VALUES ('100001', '73', '土耳其', '1', 1, 1, 73);
INSERT INTO `t_code` VALUES ('100001', '74', '乌兹别克斯坦', '1', 1, 1, 74);
INSERT INTO `t_code` VALUES ('100001', '75', '哈萨克斯坦', '1', 1, 1, 75);
INSERT INTO `t_code` VALUES ('100001', '76', '吉尔吉斯', '1', 1, 1, 76);
INSERT INTO `t_code` VALUES ('100001', '77', '塔吉克斯坦', '1', 1, 1, 77);
INSERT INTO `t_code` VALUES ('100001', '78', '亚美尼亚', '1', 1, 1, 78);
INSERT INTO `t_code` VALUES ('100001', '79', '土库曼斯坦', '1', 1, 1, 79);
INSERT INTO `t_code` VALUES ('100001', '80', '阿塞拜疆', '1', 1, 1, 80);
INSERT INTO `t_code` VALUES ('100001', '81', '格鲁吉亚', '1', 1, 1, 81);
INSERT INTO `t_code` VALUES ('100001', '82', '埃及', '1', 1, 1, 82);
INSERT INTO `t_code` VALUES ('100001', '83', '利比亚', '1', 1, 1, 83);
INSERT INTO `t_code` VALUES ('100001', '84', '突尼斯', '1', 1, 1, 84);
INSERT INTO `t_code` VALUES ('100001', '85', '阿尔及利亚', '1', 1, 1, 85);
INSERT INTO `t_code` VALUES ('100001', '86', '摩洛哥', '1', 1, 1, 86);
INSERT INTO `t_code` VALUES ('100001', '87', '撒哈拉', '1', 1, 1, 87);
INSERT INTO `t_code` VALUES ('100001', '88', '毛里塔尼亚', '1', 1, 1, 88);
INSERT INTO `t_code` VALUES ('100001', '89', '塞内加尔', '1', 1, 1, 89);
INSERT INTO `t_code` VALUES ('100001', '90', '冈比亚', '1', 1, 1, 90);
INSERT INTO `t_code` VALUES ('100001', '91', '马里', '1', 1, 1, 91);
INSERT INTO `t_code` VALUES ('100001', '92', '布基纳法索', '1', 1, 1, 92);
INSERT INTO `t_code` VALUES ('100001', '93', '佛得角', '1', 1, 1, 93);
INSERT INTO `t_code` VALUES ('100001', '94', '几内亚比绍', '1', 1, 1, 94);
INSERT INTO `t_code` VALUES ('100001', '95', '几内亚', '1', 1, 1, 95);
INSERT INTO `t_code` VALUES ('100001', '96', '塞拉利昂', '1', 1, 1, 96);
INSERT INTO `t_code` VALUES ('100001', '97', '利比里亚', '1', 1, 1, 97);
INSERT INTO `t_code` VALUES ('100001', '98', '科特迪瓦', '1', 1, 1, 98);
INSERT INTO `t_code` VALUES ('100001', '99', '加纳', '1', 1, 1, 99);
INSERT INTO `t_code` VALUES ('100001', '100', '多哥', '1', 1, 1, 100);
INSERT INTO `t_code` VALUES ('100001', '101', '贝宁', '1', 1, 1, 101);
INSERT INTO `t_code` VALUES ('100001', '102', '尼日尔', '1', 1, 1, 102);
INSERT INTO `t_code` VALUES ('100001', '103', '尼日利亚', '1', 1, 1, 103);
INSERT INTO `t_code` VALUES ('100001', '104', '喀麦隆', '1', 1, 1, 104);
INSERT INTO `t_code` VALUES ('100001', '105', '赤道几内亚', '1', 1, 1, 105);
INSERT INTO `t_code` VALUES ('100001', '106', '乍得', '1', 1, 1, 106);
INSERT INTO `t_code` VALUES ('100001', '107', '中非', '1', 1, 1, 107);
INSERT INTO `t_code` VALUES ('100001', '108', '苏丹', '1', 1, 1, 108);
INSERT INTO `t_code` VALUES ('100001', '109', '埃塞俄比亚', '1', 1, 1, 109);
INSERT INTO `t_code` VALUES ('100001', '110', '吉布提', '1', 1, 1, 110);
INSERT INTO `t_code` VALUES ('100001', '111', '索马里', '1', 1, 1, 111);
INSERT INTO `t_code` VALUES ('100001', '112', '肯尼亚', '1', 1, 1, 112);
INSERT INTO `t_code` VALUES ('100001', '113', '乌干达', '1', 1, 1, 113);
INSERT INTO `t_code` VALUES ('100001', '114', '坦桑尼亚', '1', 1, 1, 114);
INSERT INTO `t_code` VALUES ('100001', '115', '卢旺达', '1', 1, 1, 115);
INSERT INTO `t_code` VALUES ('100001', '116', '布隆迪', '1', 1, 1, 116);
INSERT INTO `t_code` VALUES ('100001', '117', '民主刚果', '1', 1, 1, 117);
INSERT INTO `t_code` VALUES ('100001', '118', '刚果', '1', 1, 1, 118);
INSERT INTO `t_code` VALUES ('100001', '119', '加蓬', '1', 1, 1, 119);
INSERT INTO `t_code` VALUES ('100001', '120', '圣多美和普林西比', '1', 1, 1, 120);
INSERT INTO `t_code` VALUES ('100001', '121', '安哥拉', '1', 1, 1, 121);
INSERT INTO `t_code` VALUES ('100001', '122', '赞比亚', '1', 1, 1, 122);
INSERT INTO `t_code` VALUES ('100001', '123', '马拉维', '1', 1, 1, 123);
INSERT INTO `t_code` VALUES ('100001', '124', '莫桑比克', '1', 1, 1, 124);
INSERT INTO `t_code` VALUES ('100001', '125', '科摩罗伊斯兰', '1', 1, 1, 125);
INSERT INTO `t_code` VALUES ('100001', '126', '马达加斯加', '1', 1, 1, 126);
INSERT INTO `t_code` VALUES ('100001', '127', '塞舌尔', '1', 1, 1, 127);
INSERT INTO `t_code` VALUES ('100001', '128', '毛里求斯', '1', 1, 1, 128);
INSERT INTO `t_code` VALUES ('100001', '129', '津巴布韦', '1', 1, 1, 129);
INSERT INTO `t_code` VALUES ('100001', '130', '博茨瓦纳', '1', 1, 1, 130);
INSERT INTO `t_code` VALUES ('100001', '131', '纳米比亚', '1', 1, 1, 131);
INSERT INTO `t_code` VALUES ('100001', '132', '南非', '1', 1, 1, 132);
INSERT INTO `t_code` VALUES ('100001', '133', '斯威士兰', '1', 1, 1, 133);
INSERT INTO `t_code` VALUES ('100001', '134', '莱索托', '1', 1, 1, 134);
INSERT INTO `t_code` VALUES ('100001', '135', '厄立特尼亚', '1', 1, 1, 135);
INSERT INTO `t_code` VALUES ('100001', '136', '冰岛', '1', 1, 1, 136);
INSERT INTO `t_code` VALUES ('100001', '137', '挪威', '1', 1, 1, 137);
INSERT INTO `t_code` VALUES ('100001', '138', '乌克兰', '1', 1, 1, 138);
INSERT INTO `t_code` VALUES ('100001', '139', '白俄罗斯', '1', 1, 1, 139);
INSERT INTO `t_code` VALUES ('100001', '140', '摩尔多瓦', '1', 1, 1, 140);
INSERT INTO `t_code` VALUES ('100001', '141', '列支敦士登公国', '1', 1, 1, 141);
INSERT INTO `t_code` VALUES ('100001', '142', '瑞士', '1', 1, 1, 142);
INSERT INTO `t_code` VALUES ('100001', '143', '摩纳哥公国', '1', 1, 1, 143);
INSERT INTO `t_code` VALUES ('100001', '144', '安道尔公国', '1', 1, 1, 144);
INSERT INTO `t_code` VALUES ('100001', '145', '梵蒂冈', '1', 1, 1, 145);
INSERT INTO `t_code` VALUES ('100001', '146', '圣马利诺', '1', 1, 1, 146);
INSERT INTO `t_code` VALUES ('100001', '147', '克罗地亚', '1', 1, 1, 147);
INSERT INTO `t_code` VALUES ('100001', '148', '波斯尼亚和黑塞哥维那', '1', 1, 1, 148);
INSERT INTO `t_code` VALUES ('100001', '149', '马其顿', '1', 1, 1, 149);
INSERT INTO `t_code` VALUES ('100001', '150', '塞尔维亚和黑山', '1', 1, 1, 150);
INSERT INTO `t_code` VALUES ('100001', '151', '阿尔巴尼亚', '1', 1, 1, 151);
INSERT INTO `t_code` VALUES ('100001', '152', '新西兰', '1', 1, 1, 152);
INSERT INTO `t_code` VALUES ('100001', '153', '巴布亚新几内亚独立国', '1', 1, 1, 153);
INSERT INTO `t_code` VALUES ('100001', '154', '所罗门群岛', '1', 1, 1, 154);
INSERT INTO `t_code` VALUES ('100001', '155', '瓦努阿图', '1', 1, 1, 155);
INSERT INTO `t_code` VALUES ('100001', '156', '斐济群岛', '1', 1, 1, 156);
INSERT INTO `t_code` VALUES ('100001', '157', '基里巴斯', '1', 1, 1, 157);
INSERT INTO `t_code` VALUES ('100001', '158', '瑙鲁', '1', 1, 1, 158);
INSERT INTO `t_code` VALUES ('100001', '159', '密克罗尼西亚联邦', '1', 1, 1, 159);
INSERT INTO `t_code` VALUES ('100001', '160', '马绍尔群岛', '1', 1, 1, 160);
INSERT INTO `t_code` VALUES ('100001', '161', '图瓦卢', '1', 1, 1, 161);
INSERT INTO `t_code` VALUES ('100001', '162', '萨摩亚独立国', '1', 1, 1, 162);
INSERT INTO `t_code` VALUES ('100001', '163', '纽埃', '1', 1, 1, 163);
INSERT INTO `t_code` VALUES ('100001', '164', '帕劳', '1', 1, 1, 164);
INSERT INTO `t_code` VALUES ('100001', '165', '库克群岛', '1', 1, 1, 165);
INSERT INTO `t_code` VALUES ('100001', '166', '汤加', '1', 1, 1, 166);
INSERT INTO `t_code` VALUES ('100001', '167', '墨西哥', '1', 1, 1, 167);
INSERT INTO `t_code` VALUES ('100001', '168', '危地马拉', '1', 1, 1, 168);
INSERT INTO `t_code` VALUES ('100001', '169', '伯里兹', '1', 1, 1, 169);
INSERT INTO `t_code` VALUES ('100001', '170', '萨尔瓦多', '1', 1, 1, 170);
INSERT INTO `t_code` VALUES ('100001', '171', '洪都拉斯', '1', 1, 1, 171);
INSERT INTO `t_code` VALUES ('100001', '172', '尼加拉瓜', '1', 1, 1, 172);
INSERT INTO `t_code` VALUES ('100001', '173', '哥斯达黎加', '1', 1, 1, 173);
INSERT INTO `t_code` VALUES ('100001', '174', '巴拿马', '1', 1, 1, 174);
INSERT INTO `t_code` VALUES ('100001', '175', '巴哈马', '1', 1, 1, 175);
INSERT INTO `t_code` VALUES ('100001', '176', '古巴', '1', 1, 1, 176);
INSERT INTO `t_code` VALUES ('100001', '177', '牙买加', '1', 1, 1, 177);
INSERT INTO `t_code` VALUES ('100001', '178', '海地', '1', 1, 1, 178);
INSERT INTO `t_code` VALUES ('100001', '179', '多米尼加', '1', 1, 1, 179);
INSERT INTO `t_code` VALUES ('100001', '180', '圣基茨和尼维斯联邦', '1', 1, 1, 180);
INSERT INTO `t_code` VALUES ('100001', '181', '安提瓜和巴布达', '1', 1, 1, 181);
INSERT INTO `t_code` VALUES ('100001', '182', '多米尼克国', '1', 1, 1, 182);
INSERT INTO `t_code` VALUES ('100001', '183', '圣卢西亚', '1', 1, 1, 183);
INSERT INTO `t_code` VALUES ('100001', '184', '圣文森特和格林纳丁斯', '1', 1, 1, 184);
INSERT INTO `t_code` VALUES ('100001', '185', '巴巴多斯', '1', 1, 1, 185);
INSERT INTO `t_code` VALUES ('100001', '186', '格林纳达', '1', 1, 1, 186);
INSERT INTO `t_code` VALUES ('100001', '187', '特立尼达和多巴哥', '1', 1, 1, 187);
INSERT INTO `t_code` VALUES ('100001', '188', '哥伦比亚', '1', 1, 1, 188);
INSERT INTO `t_code` VALUES ('100001', '189', '委内瑞拉玻利瓦尔', '1', 1, 1, 189);
INSERT INTO `t_code` VALUES ('100001', '190', '圭亚那合作', '1', 1, 1, 190);
INSERT INTO `t_code` VALUES ('100001', '191', '苏里南', '1', 1, 1, 191);
INSERT INTO `t_code` VALUES ('100001', '192', '厄瓜多尔', '1', 1, 1, 192);
INSERT INTO `t_code` VALUES ('100001', '193', '秘鲁', '1', 1, 1, 193);
INSERT INTO `t_code` VALUES ('100001', '194', '巴西联邦', '1', 1, 1, 194);
INSERT INTO `t_code` VALUES ('100001', '195', '玻利维亚', '1', 1, 1, 195);
INSERT INTO `t_code` VALUES ('100001', '196', '智利', '1', 1, 1, 196);
INSERT INTO `t_code` VALUES ('100001', '197', '阿根廷', '1', 1, 1, 197);
INSERT INTO `t_code` VALUES ('100001', '198', '巴拉圭', '1', 1, 1, 198);
INSERT INTO `t_code` VALUES ('100001', '199', '乌拉圭东岸', '1', 1, 1, 199);
INSERT INTO `t_code` VALUES ('100002', '1', '北京市', '1', 1, 1, 1);
INSERT INTO `t_code` VALUES ('100002', '2', '天津市', '1', 1, 1, 2);
INSERT INTO `t_code` VALUES ('100002', '3', '河北省', '1', 1, 1, 3);
INSERT INTO `t_code` VALUES ('100002', '4', '山西省', '1', 1, 1, 4);
INSERT INTO `t_code` VALUES ('100002', '5', '内蒙古自治区', '1', 1, 1, 5);
INSERT INTO `t_code` VALUES ('100002', '6', '辽宁省', '1', 1, 1, 6);
INSERT INTO `t_code` VALUES ('100002', '7', '吉林省', '1', 1, 1, 7);
INSERT INTO `t_code` VALUES ('100002', '8', '黑龙江省', '1', 1, 1, 8);
INSERT INTO `t_code` VALUES ('100002', '9', '上海市', '1', 1, 1, 9);
INSERT INTO `t_code` VALUES ('100002', '10', '江苏省', '1', 1, 1, 10);
INSERT INTO `t_code` VALUES ('100002', '11', '浙江省', '1', 1, 1, 11);
INSERT INTO `t_code` VALUES ('100002', '12', '安徽省', '1', 1, 1, 12);
INSERT INTO `t_code` VALUES ('100002', '13', '福建省', '1', 1, 1, 13);
INSERT INTO `t_code` VALUES ('100002', '14', '江西省', '1', 1, 1, 14);
INSERT INTO `t_code` VALUES ('100002', '15', '山东省', '1', 1, 1, 15);
INSERT INTO `t_code` VALUES ('100002', '16', '河南省', '1', 1, 1, 16);
INSERT INTO `t_code` VALUES ('100002', '17', '湖北省', '1', 1, 1, 17);
INSERT INTO `t_code` VALUES ('100002', '18', '湖南省', '1', 1, 1, 18);
INSERT INTO `t_code` VALUES ('100002', '19', '广东省', '1', 1, 1, 19);
INSERT INTO `t_code` VALUES ('100002', '20', '广西壮族自治区', '1', 1, 1, 20);
INSERT INTO `t_code` VALUES ('100002', '21', '海南省', '1', 1, 1, 21);
INSERT INTO `t_code` VALUES ('100002', '22', '重庆市', '1', 1, 1, 22);
INSERT INTO `t_code` VALUES ('100002', '23', '四川省', '1', 1, 1, 23);
INSERT INTO `t_code` VALUES ('100002', '24', '贵州省', '1', 1, 1, 24);
INSERT INTO `t_code` VALUES ('100002', '25', '云南省', '1', 1, 1, 25);
INSERT INTO `t_code` VALUES ('100002', '26', '西藏自治区', '1', 1, 1, 26);
INSERT INTO `t_code` VALUES ('100002', '27', '陕西省', '1', 1, 1, 27);
INSERT INTO `t_code` VALUES ('100002', '28', '甘肃省', '1', 1, 1, 28);
INSERT INTO `t_code` VALUES ('100002', '29', '青海省', '1', 1, 1, 29);
INSERT INTO `t_code` VALUES ('100002', '30', '宁夏回族自治区', '1', 1, 1, 30);
INSERT INTO `t_code` VALUES ('100002', '31', '新疆维吾尔自治区', '1', 1, 1, 31);
INSERT INTO `t_code` VALUES ('100003', '1', '男装', '1', 1, 1, 1);
INSERT INTO `t_code` VALUES ('100003', '2', '女装', '1', 1, 1, 2);
INSERT INTO `t_code` VALUES ('100003', '3', '男鞋', '1', 1, 1, 3);
INSERT INTO `t_code` VALUES ('100003', '4', '女鞋', '1', 1, 1, 4);
INSERT INTO `t_code` VALUES ('100003', '5', '内衣配饰', '1', 1, 1, 5);
INSERT INTO `t_code` VALUES ('100003', '6', '箱包手袋', '1', 1, 1, 6);
INSERT INTO `t_code` VALUES ('100003', '7', '美妆护肤', '1', 1, 1, 7);
INSERT INTO `t_code` VALUES ('100003', '8', '个护清洁', '1', 1, 1, 8);
INSERT INTO `t_code` VALUES ('100003', '9', '童装', '1', 1, 1, 9);
INSERT INTO `t_code` VALUES ('100003', '10', '童鞋', '1', 1, 1, 10);
INSERT INTO `t_code` VALUES ('100003', '11', '流行', '1', 1, 1, 11);
INSERT INTO `t_code` VALUES ('100003', '12', '运动', '1', 1, 1, 12);
INSERT INTO `t_code` VALUES ('10004', '1', 'S/165', '1', 1, 1, 1);
INSERT INTO `t_code` VALUES ('10004', '2', 'M/170', '1', 1, 1, 2);
INSERT INTO `t_code` VALUES ('10004', '3', 'XL/180', '1', 1, 1, 3);
INSERT INTO `t_code` VALUES ('10004', '4', 'XXL/185', '1', 1, 1, 4);
INSERT INTO `t_code` VALUES ('10004', '5', 'XXXL/190', '1', 1, 1, 5);
INSERT INTO `t_code` VALUES ('10005', '1', '不分期', '1', 1, 1, 1);
INSERT INTO `t_code` VALUES ('10005', '2', '3期', '1', 1, 1, 2);
INSERT INTO `t_code` VALUES ('10005', '3', '6期', '1', 1, 1, 3);
INSERT INTO `t_code` VALUES ('10005', '4', '12期', '1', 1, 1, 4);
INSERT INTO `t_code` VALUES ('10005', '5', '24期', '1', 1, 1, 5);
INSERT INTO `t_code` VALUES ('10006', '1', '男', '1', 1, 1, 1);
INSERT INTO `t_code` VALUES ('10006', '2', '女', '1', 2, 1, 2);
INSERT INTO `t_code` VALUES ('100003', '13', '潮流时尚', '1', 1, 1, 13);
INSERT INTO `t_code` VALUES ('10007', '1', '花花公子', '1', 1, 1, 1);
INSERT INTO `t_code` VALUES ('10007', '2', '七匹狼', '1', 1, 1, 2);
INSERT INTO `t_code` VALUES ('10007', '3', '海澜之家', '1', 1, 1, 3);
INSERT INTO `t_code` VALUES ('10007', '4', '吉普', '1', 1, 1, 4);
INSERT INTO `t_code` VALUES ('10007', '5', '富贵鸟', '1', 1, 1, 5);
INSERT INTO `t_code` VALUES ('10007', '6', '南极人', '1', 1, 1, 6);
INSERT INTO `t_code` VALUES ('10007', '7', '红豆', '1', 1, 1, 7);
INSERT INTO `t_code` VALUES ('10007', '8', '罗蒙', '1', 1, 1, 8);
INSERT INTO `t_code` VALUES ('10007', '9', '卡玛', '1', 1, 1, 9);
INSERT INTO `t_code` VALUES ('10008', '1', '商务休闲', '1', 1, 1, 1);
INSERT INTO `t_code` VALUES ('10008', '2', '青春休闲', '1', 1, 1, 2);
INSERT INTO `t_code` VALUES ('10008', '3', '休闲风', '1', 1, 1, 3);
INSERT INTO `t_code` VALUES ('10008', '4', '街头', '1', 1, 1, 4);
INSERT INTO `t_code` VALUES ('10008', '5', '基础大众', '1', 1, 1, 5);
INSERT INTO `t_code` VALUES ('10008', '6', '原创设计', '1', 1, 1, 6);
INSERT INTO `t_code` VALUES ('10008', '7', '商务正装', '1', 1, 1, 7);
INSERT INTO `t_code` VALUES ('10008', '8', '中国风', '1', 1, 1, 8);
INSERT INTO `t_code` VALUES ('10008', '9', '嘻哈', '1', 1, 1, 9);
INSERT INTO `t_code` VALUES ('10009', '1', '青年', '1', 1, 1, 1);
INSERT INTO `t_code` VALUES ('10009', '2', '青少年', '1', 1, 1, 2);
INSERT INTO `t_code` VALUES ('10009', '3', '中年', '1', 1, 1, 3);
INSERT INTO `t_code` VALUES ('10009', '4', '情侣', '1', 1, 1, 4);
INSERT INTO `t_code` VALUES ('10009', '5', '老年', '1', 1, 1, 5);

-- ----------------------------
-- Table structure for t_codetype
-- ----------------------------
DROP TABLE IF EXISTS `t_codetype`;
CREATE TABLE `t_codetype`  (
  `C_ID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '编号',
  `C_NAME` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `N_SFKWH` int(11) NULL DEFAULT NULL COMMENT '是否可维护',
  `N_VAILD` int(11) NULL DEFAULT NULL COMMENT '是否有效'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '单值代码类型表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_codetype
-- ----------------------------
INSERT INTO `t_codetype` VALUES ('10000', '是否', 0, 1);
INSERT INTO `t_codetype` VALUES ('10001', '国家', 0, 1);
INSERT INTO `t_codetype` VALUES ('10002', '省份', 0, 1);
INSERT INTO `t_codetype` VALUES ('10003', '商品类型', 0, 1);
INSERT INTO `t_codetype` VALUES ('10004', '尺码', 0, 1);
INSERT INTO `t_codetype` VALUES ('10005', '分期类型', 0, 1);
INSERT INTO `t_codetype` VALUES ('10006', '性别', 0, 1);
INSERT INTO `t_codetype` VALUES ('10007', '品牌', 0, 1);
INSERT INTO `t_codetype` VALUES ('10008', '风格', 0, 1);
INSERT INTO `t_codetype` VALUES ('10009', '适应人群', 0, 1);

SET FOREIGN_KEY_CHECKS = 1;
